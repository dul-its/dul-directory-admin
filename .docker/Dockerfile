ARG RUBY_VERSION=3.2.6
FROM image-mirror-prod-registry.cloud.duke.edu/library/ruby:${RUBY_VERSION}

# Used for production image
# Not suitable for testing

USER 0

RUN apt-get update \
	&& apt-get install -y --no-install-recommends \
	   mariadb-client \
     nodejs \
     npm \
		 netcat-traditional \
		 ldap-utils \
		 wait-for-it \
		 shared-mime-info \
	&& rm -rf /var/lib/apt/lists/*

ENV GEM_HOME="/usr/local/bundle"
ENV PATH $GEM_HOME/bin:$GEM_HOME/gems/bin:$PATH

RUN bundle config --global frozen 1
WORKDIR /usr/src/app
COPY Gemfile* ./
RUN gem install bundler
RUN bundle install

COPY . .

# Account for any needed
# cron jobs
COPY .docker/cron.d/ /etc/cron.d/

RUN npm install -g yarn
RUN yarn install

# before running assets:precompile, DUL_LDAP_BINDDN needs to be
# declared as an ENV variable, because one of the initializers
# is looking for the var
ENV DUL_LDAP_BINDDN=""
ENV DUL_LDAP_BINDPW=""
RUN bundle exec rake assets:precompile

COPY .docker/docker-entrypoint.sh /usr/local/bin
COPY .docker/execpod.sh /usr/local/bin

# The idea here is to ensure the DevOps "deployment" 
# version of 'database.yml' is included in the image, 
# while allowing developers to use their own version 
# locally with little to no disruption
COPY .docker/database.yml config/database.yml

RUN chmod a+x /usr/local/bin/*
RUN chmod -R a+rw /usr/src/app
RUN mkdir /.cache
RUN chmod -R a+rw /.cache
RUN mkdir /.yarnrc
RUN chmod -R a+rw /.yarnrc

RUN chmod a+rw /tmp

ENTRYPOINT ["docker-entrypoint.sh"]

USER 1001
EXPOSE 3000

CMD ["start"]
