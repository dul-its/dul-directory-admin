# Docker-related Files 

We use the `docker-entrypoint.test.sh` and `docker-compose.test.yml` files during the build image stage.  These files are not used when deploying on either **stage** or **production**.  