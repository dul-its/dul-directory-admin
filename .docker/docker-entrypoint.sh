#!/bin/bash
# https://stackoverflow.com/a/38732187/1935918
set -e

if [ -f /usr/src/app/tmp/pids/server.pid ]; then
  rm /usr/src/app/tmp/pids/server.pid
fi

case $1 in
  start)
    unset BUNDLE_PATH
    unset BUNDLE_BIN

    wait-for-it ${DATABASE_HOST:-db}:${DATABASE_PORT:-3306} -t 30 --

    # ensure we have admin users ready
    if [ "$RAILS_ENV" == "production" ]; then
      bundle exec rake db:migrate 2>/dev/null || bundle exec rake db:setup
      bin/rails user:create:staging_admin_users
    else
      bundle exec rake db:migrate:reset
      bundle exec rake db:seed
      bin/rails user:create:development_users
    fi

    exec bundle exec rails server -b 0.0.0.0 -p 3000
    ;;

  rspec)
    wait-for-it ${DATABASE_HOST:-db}:${DATABASE_PORT:-3306} -t 60 --
    # yarn install
    bin/rails db:migrate:reset
    # bin/rails db:setup
    # bin/rails staff_photo:attach 2>/dev/null
    # bin/rails user:create:development_users 2>/dev/null
    exec bundle exec rake spec
    ;;

  rubocop)
    exec bundle exec rubocop --cache false
    ;;

  update)
    exec bundle update
    ;;

  shell)
    exec /bin/bash
    ;;

  *)
    exec "$@"
    ;;
esac

echo "enjoy the ride..."
exec "$@"
