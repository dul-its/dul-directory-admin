#!/bin/bash

#/bin/bash -c "
#	while ! nc -z db 3306;
#	do
#		echo sleeping;
#		sleep 1;
#	done;
#	echo Connected to database!;
#"

wait-for-it db:3306 -t 30
bundle install

rake db:setup 2>/dev/null
rake db:migrate
rake staff:refresh_from_ldap

exec "$@"
