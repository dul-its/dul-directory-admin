#!/bin/bash

set -e

bundle exec rake db:environment:set RAILS_ENV=production
bundle exec rake db:migrate RAILS_ENV=production 2>/dev/null || bundle exec rake db:setup RAILS_ENV=production

exec "$@"

