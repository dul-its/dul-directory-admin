# This Dockerfile is responsible for generating/building
# our "test" image.
# 
# It uses a version of docker-entrypoint.sh that assumes a local
# database container, and waits for its readiness (via 'wait-for-it')
#
FROM image-mirror-prod-registry.cloud.duke.edu/library/ruby:3.2.6

USER 0

RUN apt-get update -qq \
      && apt-get install -y --no-install-recommends \
      nodejs \
      npm \
      shared-mime-info \
      ldap-utils \
      netcat-traditional wait-for-it \
      && rm -rf /var/lib/apt/lists/*

RUN npm install -g yarn

WORKDIR /usr/src/app
COPY Gemfile ./
RUN gem install bundler
RUN bundle install

COPY . .

# before running assets:precompile, DUL_LDAP_BINDDN needs to be
# declared as an ENV variable, because one of the initializers
# is looking for the var
ENV DUL_LDAP_BINDDN=""
ENV DUL_LDAP_BINDPW=""
RUN bundle exec rake assets:precompile

# COPY .docker/docker-entrypoint.sh /usr/local/bin/
COPY .docker/docker-entrypoint.sh /usr/local/bin/
RUN chmod a+x /usr/local/bin/docker-entrypoint.sh

ENTRYPOINT ["docker-entrypoint.sh"]

USER 1001
EXPOSE 3000

CMD ["start"]
