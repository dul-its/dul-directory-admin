# frozen_string_literal: true

source 'https://rubygems.org'
git_source(:github) { |repo| "https://github.com/#{repo}.git" }

ruby '3.2.6'

# Bundle edge Rails instead: gem 'rails', github: 'rails/rails'
gem 'rails', '~> 7.2'
# Use sqlite3 as the database for Active Record
gem 'mysql2'
# gem 'pg'
# gem 'sqlite3'

# Use Puma as the app server
gem 'puma', '~> 5.6'
# Use SCSS for stylesheets
gem 'sassc-rails'
# Use Uglifier as compressor for JavaScript assets
gem 'uglifier', '>= 1.3.0'
# See https://github.com/rails/execjs#readme for more supported runtimes
# gem 'mini_racer', platforms: :ruby

# Use CoffeeScript for .coffee assets and views
gem 'coffee-rails', '~> 4.2'
# Turbolinks makes navigating your web application faster. Read more: https://github.com/turbolinks/turbolinks
# gem 'turbolinks', '~> 5'
# Build JSON APIs with ease. Read more: https://github.com/rails/jbuilder
gem 'jbuilder', '~> 2.5'
# Use Redis adapter to run Action Cable in production
# gem 'redis', '~> 4.0'
# Use ActiveModel has_secure_password
# gem 'bcrypt', '~> 3.1.7'

# Use ActiveStorage variant
# gem 'mini_magick', '~> 4.8'

# Use Capistrano for deployment
# gem 'capistrano-rails', group: :development

# Reduces boot times through caching; required in config/boot.rb
gem 'bootsnap', '>= 1.13.0', require: false

# toggle this version of dul_bootstrap_theme for local development
# gem 'dul_bootstrap_theme', path: '/app/vendor/dul_bootstrap_theme'
gem 'dul_bootstrap_theme',
    git: 'https://gitlab.oit.duke.edu/dul-its/dul_bootstrap_theme/',
    tag: 'v1.1.4'

gem 'ancestry'
gem 'bootstrap', '~> 4.6'
gem 'breadcrumbs_on_rails'
gem 'cancancan', '~> 3.3'
gem 'ckeditor'
gem 'devise'
gem 'execjs'
gem 'net-ldap'
gem 'net-pop', require: false
gem 'net-smtp'
gem 'nokogiri', '~> 1.14'
gem 'omniauth', '< 2'
gem 'omniauth-shibboleth'
gem 'rails_admin', '~> 3.0'
gem 'rails_admin_nestable',
    git: 'https://gitlab.oit.duke.edu/dul-its/rails_admin_nestable/'

gem 'sass-rails', '~> 6'
gem 'seed_dump'
gem 'sprockets', '~> 4'

# for active_storage image uploads
gem 'active_storage_validations'
gem 'image_processing', '~> 1.2'
gem 'mini_magick'

group :development, :test do
  # Call 'byebug' anywhere in the code to stop execution and get a debugger console
  gem 'byebug', platforms: %i[mri mingw x64_mingw]
  gem 'factory_bot_rails'
  gem 'rails-controller-testing'
  gem 'rspec-rails', '~> 7'
  gem 'rubocop', '~> 1.0'
  gem 'rubocop-rspec', '~> 3'
end

group :development do
  # Access an interactive console on exception pages or by calling 'console' anywhere in the code.
  # gem 'listen', '>= 3.0.5', '< 3.2'
  gem 'listen', '~> 3.7'
  gem 'web-console', '>= 3.3.0'
end

group :test do
  # Adds support for Capybara system testing and selenium driver
  gem 'capybara', '~> 3.4'
  gem 'selenium-webdriver'
  gem 'webdrivers'
end

# Windows does not include zoneinfo files, so bundle the tzinfo-data gem
gem 'tzinfo-data', platforms: %i[mingw mswin x64_mingw jruby]
