# REFRESH STAFF DATA FROM LDAP

Updates to the staff directory from data provided by Duke's LDAP database occur each morning at 6AM.
  
### Trigger Manual Task
In the event of a need to run the task independent of the regularly scheduled 6AM run, one can SSH into **staffdir-01** and do this:
```bash
ssh staffdir-01.lib.duke.edu
curl localhost:3000/api/refresh_from_ldap
```
#### Notes
The job will fail if the Docker container (that exposes port 3000) is not running.
