# README
[[_TOC_]]

This README would normally document whatever steps are necessary to get the
application up and running. It needs a little bit of love and care...  

## Developer Notes

#### Prerequisites
* Docker
* Docker Compose
* Locally installed database service (optional)

### TL;DR
```bash
$ cd /path/to/dul-directory-admin

# The docker compose file for development is at the root of the app directory structure
$ cat docker-compose.yml

# inspect the dev.env file (optional)
$ cat dev.env

# Everything is configured the way you want...? Then fire it up!
$ [sudo] docker-compose up -d
```
#### Notes on 'docker-compose.yml'
* The `docker-compose.yml` loads environment variables from `dev.env`, notably `DATABASE_URL` and `RAILS_ENV`.
* A `mariadb` database image is included with a service name of `db`.

#### RAILS_ENV
You'll need to set `RAILS_ENV=development` if you're running the database container included in the `docker-compose.yml` file.  

### Visit The Browser
Visit http://localhost:3000 or http://yourhost:3000

### Add seed data to app
* In the same terminal tab as above, add staff data to the app by running:
```
rake db:seed:replant
``` 
* To attach photos to staff, run: 
```
rake staff_photo:attach
```
* And to make sure the app has permission to access all ActiveStorage variants (derivatives), run: 
```
chmod -R 775 /usr/src/app/storage
```

### Create an admin user
* To gain shell access to the running container, open a new terminal tab and run: 
```
docker exec -u 0 -it dul-directory-admin-dev /bin/bash
```
* To create an admin user (admin@duke.edu) run: 
```
rake user:create:dev_admin_user
```

### View the Admin
Browse to http://localhost:3000/admin and sign in using admin@duke.edu / password

## Create Seed Data From Existing Database

```bash
# Dump database content to seed file.
$ rake db:seed:dump MODELS="User, Person, Department, ExternalContact, Language, SubjectArea, Training, Certification, Expertise, ExternalExpertise, Fluency, Role" EXCLUDE=created_at,updated_at

# Dump currently attached staff photos to file system (./lib/assets/images/staff_photos)
$ rake staff_photo:export
```

## Launch a Rails console
```bash
$ docker-compose run -e "RAILS_ENV=development" app bundle exec rails c
```

## Run tests in a container
```bash
$ docker-compose run -e "RAILS_ENV=test" app bundle exec rspec
```
## Refresh Staff Data from LDAP (Manually)
```
$ docker exec -it [container-name] rake staff:refresh_from_ldap
```
  
## Server Hosts
**Production**  
directory.library.duke.edu  
  
**Staging (or Pre-production)**  
directory-stage.lib.duke.edu  
  
## Useful Information (Operations)

### Manually Refreshing People Data (from LDAP)
See [LDAP_REFRESH.md](./LDAP_REFRESH.md), available in this repository.

## Nestable Gem
We've forked the [Rails Admin Nestable gem](https://gitlab.oit.duke.edu/dul-its/rails_admin_nestable) in order to get it working with Rails 6.

## Post-OKD Deployment

### Exporting Seed Data And Photos From Production (Using `oc`)
Note: The OpenShift Command Line program is required for this.  
  
```
# Get/copy your login token from the OKD3 Cluster UI (manage.cloud.duke.edu:443)
$ oc login ...

$ oc project dul-directory-prod
Now using project "dul-directory-prod" on server "https://manage.cloud.duke.edu:443".

$ oc get pods
NAME                                                 READY   STATUS             RESTARTS   AGE
production-mariadb-0                                 1/1     Running            1          105d
production-staff-directory-f55975d9c-2bkjp           1/1     Running            0          63d
production-staff-directory-proxy-74cdc4fd5d-hh49j    2/2     Running            0          110d

# The name of the app's pod is "production-staff-directory-f55975d9c-2bkjp".
# So next, we'll remote shell into pod:

$ oc rsh production-staff-directory-f55975d9c-2bkjp
# (You're now in the pod)
$ rake db:seed:dump MODELS="User, Person, Department, ExternalContact, Language, SubjectArea, Training, Certification, Expertise, ExternalExpertise, Fluency, Role" EXCLUDE=created_at,updated_at

# /usr/src/app/db/seeds.db is up-to-date, remotely.
$ exit

# (You're now back in your local shell)
# (From here, you'll rsync the data...)

$ cd /path/to/your/dul-directory-admin
$ mkdir export

$ oc rsync --help
# see how this command works.

$ oc rsync production-staff-directory-f55975d9c-2bkjp:/usr/src/app/db/ ./export/
# (NOTE: the trailing slashes on directory names are important here)

$ ls -la export
total 436
drwxrwxrwx 1 dlc32 dukeusers     48 Mar 14 11:07 .
drwxr-xr-x 1 dlc32 dukeusers   1036 Mar 14 11:09 ..
drwxrwxrwx 1 dlc32 dukeusers   4046 Nov  1 13:43 migrate
-rw-rw-rw- 1 dlc32 dukeusers   7890 Mar 13 13:59 schema.rb
-rw-rw-rw- 1 dlc32 dukeusers 435816 Mar 14 11:05 seeds.rb
```

Next, you can copy the seeds file into the main app: `cp export/seeds.db db/`

After that, We'll need to edit the seeds.rb file to remove all instances of `encrypted_password: "123xyz",` from the User model data. You can use this regex in Visual Studio Code to quickly find and replace (make sure to toggle to regex mode, and make sure to include the initial space character):
```  encrypted_password: "\$2a\$[^"]+",```

Finally, save the file and commit it to the repo.

Now let's repeat the process, only for Staff Photos. 

```
# remote shell back into pod:
$ oc rsh production-staff-directory-f55975d9c-2bkjp
$ rake staff_photo:export
```

The rsync process seems to sometimes mangle the last file, so we can add a zipped version of the contents to be safe:

```
$ cd /usr/src/app/lib/assets/images/staff_photos
$ tar -czf zipped_photos.tar.gz *
$ exit
```

Photos are exported, now we need to copy them down using rsync

```
# In your local setup, create an export folder for photos:
$ mkdir photos_export

# rsync the remote photos
$ oc rsync production-staff-directory-f55975d9c-2bkjp:/usr/src/app/lib/assets/images/ ./photos_export/
# (NOTE: the trailing slashes on directory names are important here)
# (ALSO NOTE: this transfer will take a long time!)
```

Now in your local setup, clean out the contents of `/lib/assets/images/staff_photos`

And replace them with what you copied into `/photos_export` (but make sure to exclude the large zipped file!)

Do a `git status` to see what's changed

Do a `git add .` and then commit to the repo

### Getting Production Data onto Staging for Review
We need to accomplish two main things to get Staging ready for review:
- Copying over the latest staff photos
- Updating Seed Data

### Getting Production Data onto Staging for Review
- merge your changes to Main branch, making sure to include the latest version of the seeds file and the exported photos
- after merge pipeline has run, the new code should be up on [stage-staff-directory](https://manage.cloud.duke.edu/console/project/dul-directory-stage/overview) in OKD Console
- click on the 'pod' icon to the right of **stage-staff-directory** and then click the **Terminal** tab
- now we will run command to update the seeds:
```rails db:seed:replant```
- and then when the above is complete, run:
```rake staff_photo:attach```
- everything should now look correct on https://directory-stage.library.duke.edu/
