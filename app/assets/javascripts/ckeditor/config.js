/* DUL Custom configuration for CKEditor WYSIWYG UI */

/* Default configs provided by CKEditor gem: */
/* https://github.com/galetahub/ckeditor/blob/master/app/assets/javascripts/ckeditor/config.js */

/* CKEditor gem custom toolbar documentation: */
/* https://github.com/galetahub/ckeditor#custom-toolbars-example */

/* CKEditor 4 toolbar configuration docs: */
/* https://ckeditor.com/docs/ckeditor4/latest/features/toolbar.html */
/* https://ckeditor.com/docs/ckeditor4/latest/guide/dev_configuration.html */

/* All CKEditor 4 custom config options: */
/* https://ckeditor.com/docs/ckeditor4/latest/api/CKEDITOR_config.html */

/* Note we are using CKEditor 4's Advanced Content Filter - Automatic Mode to filter */
/* out undesirable tags/attributes that aren't already enabled by having a button in */
/* the toolbar. Rules can be adjusted further; see: */
/* https://ckeditor.com/docs/ckeditor4/latest/guide/dev_acf.html */
/* https://ckeditor.com/docs/ckeditor4/latest/examples/acf.html */

if (typeof(CKEDITOR) != 'undefined') {
  CKEDITOR.editorConfig = function( config )
  {
    config.toolbar_duke = [
      { name: 'styles', items: [ 'Format' ] },
      { name: 'basicstyles', items: [ 'Bold', 'Italic', 'Underline' ] },
      { name: 'links', items: [ 'Link', 'Unlink' ] },
      { name: 'paragraph', items: [ 'NumberedList', 'BulletedList', 'JustifyLeft', 'JustifyCenter', 'JustifyRight' ] },
      { name: 'clipboard', items: [ 'PasteText', 'PasteFromWord' ] },
      { name: 'document', items: [ 'Source'] }
    ]
    config.format_tags = 'p;h2;h3;h4;pre';
    config.extraPlugins = 'justify';
    config.toolbar = 'duke';
  };
}
