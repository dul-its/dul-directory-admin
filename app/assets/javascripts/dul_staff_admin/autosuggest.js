$('document').ready(function() {
  var allNamesCombined = $('#autosuggest-data').data('autosuggest').split('|||');

  var options = {
    data: allNamesCombined,
    getValue: function(element) {
      // Directly return the element which is a string in this case
      return element;
    },
    template: {
      type: "custom",
      method: function(value, item) {
        // Replace {nickname} with a span that hides it
        return value.replace(/\{(.*?)\}/g, '<span class="d-none">$1</span>');
      }
    },
    theme: 'light', cssClasses: 'col',
    list: {
      match: { enabled: true },
      maxNumberOfElements: 20,
      onChooseEvent: function() {
        var target = $('#q');
        var selected = target.getSelectedItemData();
        if (selected) {
          // Set the value without the hidden nickname
          $(target).val(selected.replace(/\{(.*?)\}/g, '').trim());
        }
        $('#staff-search-form').submit();
      }
    }
  };

  $('#q').easyAutocomplete(options);
});
