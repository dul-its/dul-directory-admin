$('document').ready(function() {

  // Prevent form submit for dept search; use live-filter instead
  $("form#departments-filter").submit(function(e) {
    e.preventDefault();
  });

  // Live-filter the departments page
  $("input#department-search").on("keyup", function() {
    $("#department-filter-results").show();
    $("#departments-content").hide();

    var value = $(this).val().toLowerCase();
    $("#department-filter-results li").filter(function() {
      $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
    });

    if (value.length === 0) { // if removed the search string...
      $("#department-filter-results").hide();
      $("#departments-content").show();
    }
  });

});

