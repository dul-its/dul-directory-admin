
$(document).ready(function(){
  // remove all styles from #profile-content
  $("#profile-content").find('*').removeAttr('style');
  $("#profile-content").find('*').removeAttr('class');
  $("#profile-content span").contents().unwrap();
});
