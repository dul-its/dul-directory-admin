$('document').ready(function() {

  // Prevent form submit for subject search; use live-filter instead
  $("form#subject-specialist-filter").submit(function(e) {
    e.preventDefault();
  });

  // Live-filter the subject specialists page
  $("input#subject-search").on("keyup", function() {
    var value = $(this).val().toLowerCase();
    $("#subject-specialist-list .subject-group").filter(function() {
      $(this).toggle($(this).find('.subject-title, .person-name, .person-job-title').text().toLowerCase().indexOf(value) > -1)
    });
  });

});
