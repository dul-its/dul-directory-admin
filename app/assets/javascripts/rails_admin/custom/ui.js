/* Custom Javascript for DUL Directory Admin */
/* See https://github.com/sferik/rails_admin/wiki/Theming-and-customization */
/* ================================================================= */

/* ----------------------------------------------------------------- */
/* Make URL slugs from dept names                                    */
/* https://gist.github.com/hagemann/382adfc57adbd5af078dc93feef01fe1 */
/* ----------------------------------------------------------------- */
function slugify(string) {
  const a = 'àáäâãåăæçèéëêǵḧìíïîḿńǹñòóöôœøṕŕßśșțùúüûǘẃẍÿź·/_,:;'
  const b = 'aaaaaaaaceeeeghiiiimnnnooooooprssstuuuuuwxyz------'
  const p = new RegExp(a.split('').join('|'), 'g')

  return string.toString().toLowerCase()
    .replace(/\s+/g, '-') // Replace spaces with -
    .replace(p, c => b.charAt(a.indexOf(c))) // Replace special characters
    .replace(/&/g, '-and-') // Replace & with 'and'
    .replace(/[^\w\-]+/g, '') // Remove all non-word characters
    .replace(/\-\-+/g, '-') // Replace multiple - with single -
    .replace(/^-+/, '') // Trim - from start of text
    .replace(/-+$/, '') // Trim - from end of text
}

$(document).on('rails_admin.dom_ready', function(){

  /* Update the slug if the dept name gets added or changed */
  $("input#department_name").on("input propertychange", function() {
    $("input#department_slug").val(slugify($(this).val()));
  });

  /* check staff photo upload */
  const imageInput = document.getElementById('person_staff_photo');

  if (imageInput) {
    imageInput.addEventListener('change', (event) => {
      const file = event.target.files[0];
      const allowedTypes = ['image/jpeg', 'image/jpg', 'image/png'];

      if (!allowedTypes.includes(file.type)) {
        alert('Invalid file type. Please upload a JPG or PNG image.');
        clearImageInput();
        return;
      }

      const reader = new FileReader();

      reader.onload = (e) => {
        const img = new Image();
        img.src = e.target.result;

        img.onload = () => {
          const width = img.width;
          const height = img.height;

          if (width < 600 || height < 800) {
            alert('The uploaded image is too small. Please upload an image larger than 600x800 pixels.');
            clearImageInput();
          } else if (width > 1800 || height > 2400) {
            alert('The uploaded image is too large. Please upload an image smaller than 1800x2400 pixels.');
            clearImageInput();
          }
        };
      };

      reader.readAsDataURL(file);
    });
  }

  /* clear file input and thumbnail */
  function clearImageInput() {
    imageInput.value = '';
    const thumbnailElement = document.querySelector('.img-thumbnail');
    if (thumbnailElement) {
      thumbnailElement.parentNode.removeChild(thumbnailElement);
    }
  }

});
