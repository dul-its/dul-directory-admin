# frozen_string_literal: true

class ApiController < ApplicationController
  include WeeklyApi

  def show; end

  def check_membership
    return unless dul_ldap.bind

    entry = dul_ldap.membership_info
    entry.inspect
  end

  def refresh_from_ldap
    # several tasks need to happen in this space
    # 1) load all of the Person (model) instances
    #    from the database
    #
    #
    # 3) With the established LDAP connection,
    #    query for all persons who are "isMemberOf" of:
    #    urn:mace:duke.edu:groups:library:perkins
    #
    #    ..and whose primary affiliation is either staff or faculty.
    #
    # This may involve two 'loop' passes
    # - one to determine if there are new people (from the LDAP feed)
    # - another to determine if there existing People that are 'not in the LDAP feed
    
    additions = []
    suppressions = []

    if dul_ldap.bind

      # fetch all LDAP entries representing persons are under the broad Perkins group, AND
      # who are either staff or faculty.
      entries = dul_ldap.staff_entries
      return if entries.nil?

      # these are the known active staff (from LDAP)
      # in which we'll add keys below (in the entries.each loop)
      known_active = {}

      found = 0

      # keep track of entries processed
      # rubocop:disable Style/TrailingCommaInHashLiteral
      stats = {
        entries_processed: 0,
        staff_added: 0,
        staff_updated: 0,
        staff_suppressed: 0,
        save_errors: 0,
      }
      # rubocop:enable Style/TrailingCommaInHashLiteral

      additions = []
      suppressions = []

      entries.each do |entry|
        known_active[entry[:uid].first.to_sym] = false

        netID = entry[:uid].first.to_s
        Rails.logger.debug 'refresh_from_ldap: Inspecting netid, %s, for changes' % netID

        person = Person.find_by(net_id: netID)
        person = Person.new if person.nil?

        helpers.refresh_person(person, entry)
        if person.new_record?
          staff_stat_key = :staff_added
          person_type = :discovered
        else
          staff_stat_key = :staff_updated
          person_type = :existing
          found += 1
        end

        begin
          person.save!
        rescue ActiveRecord::RecordNotSaved => e
          Rails.logger.error "refresh_from_ldap (save #{person_type}): #{e} (#{person.display_name})"
          stats[:save_errors] += 1
        rescue ActiveRecord::RecordInvalid => e
          Rails.logger.error "refresh_from_ldap (validation error): #{e} (#{person.display_name})"
          stats[:save_errors] += 1
        else
          Rails.logger.info "refresh_from_ldap: saved data for #{person_type} person: #{person.display_name}"
          stats[staff_stat_key] += 1
        end

        stats[:entries_processed] += 1
      end

      # 2nd Pass -- identify staff members who are no longer
      # in the LDAP feed
      Person.find_each do |existing_person|
        existing_netid = existing_person.net_id
        akey = existing_person.net_id.to_sym
        existing_display_name = existing_person.display_name

        unless known_active.key?(akey)
          Rails.logger.debug "#{existing_person.display_name} needs to be removed (not in ldap feed)"
        end

        next unless !known_active.key?(akey) && existing_person.active

        existing_person.active = false

        begin
          existing_person.save!
        rescue ActiveRecord::RecordNotSaved => e
          Rails.logger.error format('refresh_from_ldap (save existing - visibility): %s (%s)', e,
                                    existing_person.display_name)
          stats[:save_errors] += 1
        else
          stats[:staff_suppressed] += 1
          Rails.logger.warn format(
            'refresh_from_ldap: %s (%s): active for person set to 0 as he/she/it no longer exists in LDAP feed.', existing_display_name, existing_netid
          )
          suppressions << { netid: existing_netid, display_name: existing_display_name }
        end
      end
    end

    NoticeMailer.with(additions: additions, suppressions: suppressions).ldap_updates_email.deliver_now

    # report statistics (to syslog)
    s = format('refresh_from_ldap: %s|%s|%s|%s|%s|%s',
               stats[:entries_processed],
               found,
               stats[:staff_updated],
               stats[:staff_added],
               stats[:staff_suppressed],
               stats[:save_errors])
    Rails.logger.info(s)

    render plain: :Hello
  end

  private

  def dul_ldap
    # authentication config is specified in
    # lib/dul_ldap.rb, since it's Duke-related and not likely to change.
    @dul_ldap ||= DulLdap.new
  end
end
