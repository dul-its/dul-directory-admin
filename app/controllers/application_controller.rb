# frozen_string_literal: true

class ApplicationController < ActionController::Base
  rescue_from CanCan::AccessDenied, with: :forbidden

  # NOTE: This callback is triggered after login.
  #       It is overridden here to add the current user's
  #       associated person id into the session for convenience.
  def after_sign_in_path_for(resource_or_scope)
    session[:person_id] = Person.find_by(net_id: current_user.net_id)&.id

    super
  end

  def not_found
    raise ActionController::RoutingError, 'Not Found'
  end
end
