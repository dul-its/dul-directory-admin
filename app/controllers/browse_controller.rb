# frozen_string_literal: true

class BrowseController < ApplicationController
  def index
    @all_people = Person.is_active.is_valid.order(:last_name).order(:display_name)

    # breadcrumb links:
    add_breadcrumb t('public_ui.short_title'), :browse_index_path, class: 'breadcrumb-item'
  end

  def show
    redirect_to "/browse/#{params[:id].downcase}" if params[:id].present? && params[:id] != params[:id].downcase
    @all_people = Person.is_active.is_valid.order(:last_name).order(:display_name)
    @people = if params[:id].present? && params[:id].length == 1
                Person.is_active.is_valid.where('last_name LIKE ?', "#{params[:id]}%").order(:last_name).order(:display_name) || not_found
              else
                not_found
              end

    # breadcrumb links:
    add_breadcrumb t('public_ui.short_title'), :browse_index_path, class: 'breadcrumb-item'
    add_breadcrumb t('public_ui.browse.title', letter: params[:id].upcase), :browse_show_path, class: 'breadcrumb-item' if params[:id].present?
  end
end
