# frozen_string_literal: true

module WeeklyApi
  extend ActiveSupport::Concern
  included do
    def do_weekly_report
      report = {
        no_preferred_title: [],
        no_department: []
      }

      # Staff with a blank "Preferred Title"
      people = Person.where("viewable = 1 AND (preferred_title IS NULL or preferred_title = '')")
      people.find_each do |person|
        display_name = person.display_name.blank? ? "#{person.last_name}, #{person.first_name}" : person.display_name
        report[:no_preferred_title] << "#{display_name} (#{person.net_id})"
      end

      # Now do the people who don't belong to at least one department
      people = Person.where.missing(:departments)
      people.find_each do |person|
        next unless person.viewable?

        display_name = person.display_name.blank? ? "#{person.last_name}, #{person.first_name}" : person.display_name
        report[:no_department] << "#{display_name} (#{person.net_id})"
      end

      NoticeMailer.with(people_no_title: report[:no_preferred_title],
                        people_no_dept: report[:no_department])
                  .weekly_report_email
                  .deliver_now

      render plain: 'Hello Weekly Report'
    end
  end
end
