# frozen_string_literal: true

class DepartmentsController < ApplicationController
  def index
    @depts = Department.all
    @root_dept = Department.order(:id).first

    # breadcrumb links:
    add_breadcrumb t('public_ui.short_title'), :browse_index_path, class: 'breadcrumb-item'
    add_breadcrumb t('public_ui.dept.title'), :dept_index_path, class: 'breadcrumb-item'
  end

  def show
    # breadcrumb links:
    add_breadcrumb t('public_ui.short_title'), :browse_index_path, class: 'breadcrumb-item'
    add_breadcrumb t('public_ui.dept.title'), :dept_index_path, class: 'breadcrumb-item'

    @depts = Department.all

    if params[:slug].to_i.zero?
      @dept = Department.find_by(slug: params[:slug]) || not_found
      add_breadcrumb @dept.name, :dept_show_path, class: 'breadcrumb-item'
    else
      @dept = Department.find_by(id: params[:slug]) || not_found
      redirect_to dept_show_path(@dept.slug)
    end

    @root_dept = Department.order(:id).first
  end
end
