# frozen_string_literal: true

class ExecutiveGroupController < ApplicationController
  def index
    # executive group members
    eg = Person.is_active.is_valid.where(is_eg_member: true).order(:last_name).order(:display_name)

    @head_librarian = eg.select(&:head_of_library?)

    @other_eg_members = eg - @head_librarian

    # for department side nav
    @root_dept = Department.order(:id).first

    # breadcrumb links:
    add_breadcrumb t('public_ui.short_title'), :browse_index_path, class: 'breadcrumb-item'
    add_breadcrumb t('public_ui.executive_group.title'), :executive_group_index_path, class: 'breadcrumb-item'
  end
end
