# frozen_string_literal: true

class HealthController < ActionController::Base
  def index
    render status: 200
  end
end
