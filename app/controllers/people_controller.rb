# frozen_string_literal: true

class PeopleController < ApplicationController
  before_action :redirect_from_id_to_slug, only: :show

  def show
    @person = Person.is_active.is_valid.find_by(slug: params[:slug])

    not_found unless @person

    # breadcrumb links:
    add_breadcrumb t('public_ui.short_title'), :browse_index_path, class: 'breadcrumb-item'
    add_breadcrumb @person&.display_name, :staff_show_path, class: 'breadcrumb-item'
  end

  private

  def redirect_from_id_to_slug
    return if params[:slug].to_i.zero?

    person = Person.is_active.is_valid.find_by(id: params[:slug])
    redirect_to staff_show_path(person.slug) if person
  end
end
