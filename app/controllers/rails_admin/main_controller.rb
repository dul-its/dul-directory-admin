# frozen_string_literal: true

# NOTE: This overrides the default #redirect_to_on_success
#       method in RailsAdmin in order to provide a link
#       to the public view of a model when it is updated.

require_dependency RailsAdmin::Engine.config
                                     .root
                                     .join('app',
                                           'controllers',
                                           'rails_admin',
                                           'main_controller.rb').to_s

module RailsAdmin
  class MainController
    def redirect_to_on_success
      notice = I18n.t('admin.flash.successful',
                      name: @model_config.label,
                      action: I18n.t("admin.actions.#{@action.key}.done"))
      if params[:_add_another]
        redirect_to new_path(return_to: params[:return_to]), flash: { success: notice }
      elsif params[:_add_edit]
        redirect_to edit_path(id: @object.id, return_to: params[:return_to]), flash: { success: notice }
      else
        redirect_to back_or_index, flash: { success: notice }
      end
    end
  end
end
