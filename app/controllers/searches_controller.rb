# frozen_string_literal: true

class SearchesController < ApplicationController
  def search
    @query = params[:q]
    @person_results = Person.search(@query)
    @department_results = Department.search(@query)

    # breadcrumb links:
    add_breadcrumb t('public_ui.title'), :browse_index_path, class: 'breadcrumb-item'
    add_breadcrumb t('public_ui.search.title'), :search_path, class: 'breadcrumb-item'
  end
end
