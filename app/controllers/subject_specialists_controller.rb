# frozen_string_literal: true

class SubjectSpecialistsController < ApplicationController
  def index
    @subjects = SubjectArea.all

    # breadcrumb links:
    add_breadcrumb t('public_ui.short_title'), :browse_index_path, class: 'breadcrumb-item'
    add_breadcrumb t('public_ui.subject_specialists.title'), :subject_specialists_index_path, class: 'breadcrumb-item'
  end
end
