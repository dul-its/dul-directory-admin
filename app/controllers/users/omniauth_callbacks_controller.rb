# frozen_string_literal: true

require 'devise/omniauth_callbacks_controller'

module Users
  class OmniauthCallbacksController < Devise::OmniauthCallbacksController
    def shibboleth
      Rails.logger.debug '**** In OmniAuth Callback ****'
      user = resource_class.from_omniauth(request.env['omniauth.auth'])
      set_flash_message :notice, :success, kind: 'Duke NetID'
      sign_in_and_redirect user
    end
  end
end
