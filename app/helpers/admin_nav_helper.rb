# frozen_string_literal: true

module AdminNavHelper
  def current_user_greeting(current_user)
    greeting = "#{t('admin.navbar.greeting')} " \
               "#{current_user.email.gsub('@duke.edu', '')}"

    if %w[admin hr_admin subject_manager].include?(current_user.role)
      "#{greeting} (#{current_user.role})"
    else
      greeting
    end
  end

  def current_user_link(current_user, person_id = nil)
    return rails_admin.show_path(model_name: 'person', id: person_id) if person_id

    rails_admin.show_path(model_name: 'user', id: current_user.id)
  end

  def edit_your_profile_link(current_user, person_id = nil)
    return rails_admin.edit_path(model_name: 'person', id: person_id) if person_id

    rails_admin.edit_path(model_name: 'user', id: current_user.id)
  end
end
