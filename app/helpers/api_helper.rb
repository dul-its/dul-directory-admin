# frozen_string_literal: true

module ApiHelper
  def refresh_person(person, ldap_entry)
    # unique_id and net_id do not change

    # reset 'email_privacy' because, in LDAP, it's a string
    # while it's a TINYINT in the database (see below, too)
    person[:email_privacy] = 0

    DulLdap.config[:attr_field_map].each do |_model_key, ldap_key|
      # convert string to symbol
      # ldap_key_sym = ldap_key.to_sym
    end
    # p "value changes for %s: [%d]" % [ldap_entry.uid.first, value_changes]

    attr_field_map = DulLdap.config[:attr_field_map].transform_keys!(&:downcase).symbolize_keys

    ldap_entry.each do |attribute, values|
      # p "attribute from ldap_entry -- %s" % [attribute]
      # p "value from ldap_entry for %s is: %s" % [attribute, values]
      next unless attr_field_map.key? attribute.downcase.to_sym

      model_key = attr_field_map[attribute.downcase.to_sym]
      # p "model_key = [%s]" % [model_key]
      # p "%s: %s | %s: %s" % [model_key, person[model_key], attribute.downcase.to_sym, values.first]

      val_str = ''
      val_str = if values.length > 1
                  values.join('; ')
                else
                  values.first
                end

      # duUserPrivacyRequest (email_privacy)
      # requires some additional love/care.
      #
      # The presence of this attribute means we need to
      # set its mapped field (email_privacy) to 1
      if attribute.eql?('duUserPrivacyRequest')
        person[model_key] = 1
        Rails.logger.info format('refresh_from_ldap: set email_privacy=1 for %s', ldap_entry.uid.first)
      elsif person[model_key].nil? || !person[model_key].eql?(val_str)
        person[model_key] = val_str
        Rails.logger.info format('refresh_from_ldap: set %s = %s for %s', model_key, val_str, ldap_entry.uid.first)
      end
    end

    # additional processing if this is a new record...
    if person.new_record?
      person.net_id = ldap_entry.uid.first
      person.unique_id = ldap_entry.dudukeid.first
      # process the auto-generating of a new (URL) slug
      person.slug = format('%s %s', person.first_name, person.last_name)
      person.slug.downcase!
      person.slug.gsub!(/[,.]/, '')
      begin
        person.slug.gsub!(/\s/, '.').downcase
      rescue StandardError
        Rails.logger.error format('refresh_from_ldap: error with downcase on Nil object (%s %s)',
                                  person.first_name, person.last_name)
      end
    end

    # return the Person instance
    person
  end
end
