# frozen_string_literal: true

module ApplicationHelper
  def staff_photo(person:, styles: '', classes: 'media-object', wrapper_classes: 'photo', size: '')
    content_tag('div', class: wrapper_classes) do
      image_tag(staff_photo_url(person: person, size: size, internal: 'yes'),
                loading: 'lazy',
                style: styles,
                class: classes,
                alt: photo_alt(person))
    end
  end

  def staff_photo_url(person:, size: '', internal: '')
    return DulDirectoryAdmin.missing_photo_url unless person.staff_photo.attached?

    dimensions = if size == 'thumbnail'
                   '300x400'
                 else
                   '600x800'
                 end

    # SOURCE TIP:
    # https://stackoverflow.com/questions/59593069/deprecation-of-combine-options-in-active-storages-imageprocessing-transformer
    variant = person.staff_photo.variant(quality: '65',
                                         gravity: 'center',
                                         crop: '3:4',
                                         resize: dimensions)

    if internal == 'yes'
      variant
    else
      # twitter / og meta tags need a url
      rails_representation_url(variant)
    end
  end

  def libguide_link(person:)
    "#{DulDirectoryAdmin.libguide_url}#{person.libguide_id}"
  end

  def org_chart_link
    link_to 'https://library.duke.edu/about/orgchart' do
      [icon('far', 'file-pdf', 'aria-hidden' => true),
       t('public_ui.dept_nav.org_chart_title'),
       content_tag(:span, '(PDF)', class: 'sr-only')].join(' ').html_safe
    end
  end

  def link_phone_number(phone_number)
    aria_number = if phone_number.start_with?('+1 ')
                    phone_number.gsub(' ', '.').chars.join(' ')
                  else
                    phone_number.chars.join(' ').gsub('-', '.')
                  end
    link_to phone_number, "tel:#{phone_number}", 'aria-label': "call. #{aria_number}",
                                                 onclick: "_paq.push(['trackEvent', 'Directory', 'Phone', 'Phone Number']);"
  end

  private

  def photo_alt(person)
    person.staff_photo.attached? ? person.display_name : ''
  end
end
