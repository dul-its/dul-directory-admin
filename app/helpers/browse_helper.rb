# frozen_string_literal: true

module BrowseHelper
  def abc_nav(all_people)
    all_letters_array = ('a'..'z').to_a
    missing_letters_array = all_letters_array - letters_in_use(all_people)

    abc_nav_output = all_letters_array.map do |letter|
      if missing_letters_array.include?(letter)
        "<span class='btn btn-default disabled'>#{letter.upcase}</span>"
      elsif letter == params[:id]
        "<a class='btn btn-default active' href='/browse/#{letter}'>#{letter.upcase}</a>"
      else
        "<a class='btn btn-default' href='/browse/#{letter}'>#{letter.upcase}</a>"
      end
    end
    abc_nav_output.join
  end

  def letters_in_use(all_people)
    all_people.map { |person| person.last_name[0, 1].downcase if person.last_name }.compact.uniq
  end
end
