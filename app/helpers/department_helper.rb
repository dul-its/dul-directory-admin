# frozen_string_literal: true

module DepartmentHelper
  # output formatted department head
  def display_department_head(head_person)
    return unless head_person.present? && head_person.active?

    content_tag('div', class: 'dept-head') do
      concat(content_tag('span', head_person.display_name, class: 'dept-head-name'))
      concat(content_tag('span', head_person.display_title, class: 'dept-head-title'))
    end
  end

  # return the department wrapper class based on ancestry
  def department_ancestry_class(ancestry)
    n = ancestry.count('/')
    "d-flex flex-column department dept-#{n + 1}"
  end

  # return true or false based on presence of dept info
  def department_info_present(dept)
    %i[description url phone phone2 fax email campus_box
       physical_address contact_form_url].each do |attribute|
      return true if dept.public_send(attribute).present?
    end
    false
  end

  # Open Graph meta tag values (esp. for social media sharing)
  def dept_og_description(dept)
    strip_tags(dept.description&.truncate_words(50)) || ''
  end
end
