# frozen_string_literal: true

# rubocop:disable Metrics/ModuleLength
module LeftNavigationHelper
  # Generates markup for top level department cards
  def dept_nav_cards(dept, dept_tree)
    dept_tree.order('name').map do |dept_branch|
      content_tag('div', class: 'card') do
        content_tag('div',
                    id: dept_branch.slug,
                    class: card_header_class(dept, dept_branch)) do
          link_to(dept_branch.name,
                  dept_show_path(dept_branch.slug),
                  class: 'link-text') +
            link_to("#collapse-#{dept_branch.slug}",
                    class: collapse_control_class(dept, dept_branch),
                    data: { toggle: 'collapse' }) do
                      content_tag('span',
                                  class: 'sr-only') do
                                    "toggle #{dept_branch.name}"
                                  end
                    end
        end +
          content_tag('div',
                      id: "collapse-#{dept_branch.slug}",
                      class: collapse_class(dept, dept_branch)) do
            dept_nav_level2(dept, dept_branch.children) if dept_branch.children.present?
          end
      end
    end.join.html_safe
  end

  # Generates markup for 2nd level departments
  def dept_nav_level2(dept, dept_branch)
    content_tag('ul', class: 'list-group list-group-flush') do
      dept_branch.order('name').map do |dept_twig|
        content_tag('li',
                    class: dept_2_li_class(dept, dept_twig)) do
          link_to(dept_twig.name,
                  dept_show_path(dept_twig.slug),
                  class: dept_2_a_class(dept, dept_twig)) +
            link_to_if(dept_twig.children.present?,
                       content_tag('span', class: 'sr-only') do
                         "toggle #{dept_twig.name}"
                       end,
                       "#collapse-#{dept_twig.slug}",
                       class: dept_2_collapse_control_class(dept, dept_twig),
                       data: { toggle: 'collapse' }) +
            if dept_twig.children.present?
              content_tag('ul',
                          id: "collapse-#{dept_twig.slug}",
                          class: [dept_2_collapse_class(dept, dept_twig), 'dept-3']) do
                dept_nav_level3(dept, dept_twig.children)
              end
            end
        end
      end.join.html_safe
    end
  end

  # Generates markup for 3rd level departments
  def dept_nav_level3(dept, dept_twig)
    dept_twig.order('name').map do |dept_leaf|
      content_tag('li',
                  class: 'list-group-item') do
        link_to(dept_leaf.name,
                dept_show_path(dept_leaf.slug),
                class: dept_3_a_class(dept, dept_leaf)) +
          if dept_leaf.children.present?
            content_tag('ul', class: 'dept-4') do
              dept_nav_level4(dept, dept_leaf.children)
            end
          end
      end
    end.join.html_safe
  end

  # Generates markup for 4th level departments
  def dept_nav_level4(dept, dept_leaf)
    dept_leaf.order('name').map do |dept_lobe|
      content_tag('li') do
        link_to(dept_lobe.name,
                dept_show_path(dept_lobe.slug),
                class: dept_4_a_class(dept, dept_lobe))
      end
    end.join.html_safe
  end

  private

  def card_header_class(dept, dept_branch)
    active = dept == dept_branch || dept_branch.descendants.include?(dept) ? ' active' : ''
    "card-header#{active}"
  end

  def collapse_control_class(dept, dept_branch)
    control = dept_branch.children.present? ? 'collapse-control' : ''
    collapsed = dept == dept_branch || dept_branch.descendants.include?(dept) ? '' : ' collapsed'
    control + collapsed
  end

  def collapse_class(dept, dept_branch)
    show = dept == dept_branch || dept_branch.descendants.include?(dept) ? ' show' : ''
    "collapse#{show}"
  end

  def dept_2_li_class(dept, dept_twig)
    active = dept == dept_twig || dept_twig.descendants.include?(dept) ? ' active-subdept' : ''
    "list-group-item#{active}"
  end

  def dept_2_a_class(dept, dept_twig)
    dept == dept_twig || dept_twig.descendants.include?(dept) ? 'active link-text' : 'link-text'
  end

  def dept_2_collapse_control_class(dept, dept_twig)
    control = dept_twig.children.present? ? 'collapse-control' : ''
    collapsed = dept == dept_twig || dept_twig.descendants.include?(dept) ? '' : ' collapsed'
    control + collapsed
  end

  def dept_2_collapse_class(dept, dept_twig)
    show = dept == dept_twig || dept_twig.descendants.include?(dept) ? ' show' : ''
    "list-group list-group-flush collapse#{show}"
  end

  def dept_3_a_class(dept, dept_leaf)
    dept == dept_leaf || dept_leaf.descendants.include?(dept) ? 'active' : ''
  end

  def dept_4_a_class(dept, dept_lobe)
    dept == dept_lobe ? 'active' : ''
  end
end
# rubocop:enable Metrics/ModuleLength
