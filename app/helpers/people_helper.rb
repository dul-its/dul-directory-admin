# frozen_string_literal: true

module PeopleHelper # rubocop:disable Metrics/ModuleLength
  def edit_person_button(person, current_user)
    return unless current_user && current_user.net_id == person.net_id

    link_to(t('public_ui.person.edit_profile'),
            rails_admin.edit_path(model_name: 'person', id: person.id),
            class: 'btn btn-primary')
  end

  def person_attribute_list(person:, attribute:)
    case attribute
    when 'departments'
      person_list_items = person.departments.map do |dept|
        content_tag('li') do
          link_to(dept.name, dept_show_path(dept.slug))
        end
      end
    when 'languages'
      person_list_items = person.languages.map do |lang|
        content_tag('li') do
          lang.name
        end
      end
    when 'subject-areas'
      person_list_items = person.subject_areas.map do |subj|
        content_tag('li') do
          link_to(subj.title, subject_specialists_index_path(anchor: subj.id))
        end
      end
    when 'trainings'
      person_list_items = person.trainings.map do |training|
        content_tag('li', class: "training-#{training.id}") do
          link_to(training.title, training.url)
        end
      end
    end

    person_list_items.uniq.join
  end

  def person_libguide_script(person)
    return unless person.libguide_id

    '<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],' \
      "p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id))" \
      "{js=d.createElement(s);js.id=id;js.src=p+'://lgapi-us.libapps.com/widgets.php?" \
      'site_id=353&widget_type=1&search_terms=&search_match=2&sort_by=count_hit&' \
      'list_format=1&drop_text=Select+a+Guide...&output_format=1&load_type=2&' \
      'enable_description=0&enable_group_search_limit=0&enable_subject_search_limit=0' \
      "&account_ids%5B0%5D=#{person.libguide_id}&widget_title=Guide+List&widget_height=250" \
      '&widget_width=100%25&widget_link_color=2954d1&widget_embed_type=1&num_results=5&' \
      "enable_more_results=0&window_target=2&config_id=1';fjs.parentNode.insertBefore(js,fjs);}}" \
      "(document,'script','s-lg-widget-script-1');</script>"
  end

  # Open Graph meta tag values (esp. for social media sharing)
  def person_og_title(person)
    [person.display_name, person.display_title].compact.join(' | ').strip
  end

  def person_og_description(person)
    strip_tags(person_profile_display(person)&.truncate_words(50)) || ''
  end

  # Profile logic (if stored profile is empty, check for scholars at duke profile content)
  def person_profile_display(person)
    return unless person.profile.present? || scholars_at_duke_profile_content(person) != ''

    if person.profile.present?
      person.profile
    elsif scholars_at_duke_profile_content(person) != ''
      scholars_at_duke_profile_content(person)
    end
  end

  # Scholars at Duke (profile url and content)
  require 'net/http'

  def scholars_at_duke_profile_url(person)
    return unless scholars_at_duke_json(person)

    scholars_at_duke_json(person)[0]['profileURL']
  end

  def scholars_at_duke_profile_content(person)
    return unless scholars_at_duke_json(person)

    scholars_at_duke_json(person)[0]['overview']
  end

  def scholars_at_duke_json(person)
    return unless person.unique_id # require a duke unique ID to query scholars@duke

    cache_key = "person_scholars_at_duke/#{person.unique_id}"

    # NOTE: This methods gets called multiple times in the template
    #       so caching the response briefly seems to make sense.
    Rails.cache.fetch(cache_key, expires_in: 5.minutes) do
      uri = URI.parse("https://scholars.duke.edu/widgets/api/v0.9/people/contact/all.json?uri=#{person.unique_id}")
      response = Net::HTTP.get_response(uri)

      return unless valid_json?(response.body) # ensure a json response

      JSON.parse(response.body)
    end
  end

  def valid_json?(response)
    !JSON.parse(response).nil?
  rescue JSON::ParserError
    false
  end

  def in_ldap?(nid)
    ldap = DUL_Ldap.new
    ldap.bind # Consider adding error handling for bind failure

    nid_filter = Net::LDAP::Filter.eq('uid', nid)
    group_filter = Net::LDAP::Filter.construct(DUL_Ldap.config[:staff_filter])
    combined_filter = nid_filter & group_filter

    entries = ldap.connection.search(
      base: DUL_Ldap.config[:base],
      attributes: DUL_Ldap.config[:attributes],
      filter: combined_filter,
      return_result: true
    )

    entries&.any?
  rescue StandardError
    false
  end
end
