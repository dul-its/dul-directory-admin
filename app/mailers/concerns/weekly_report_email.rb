# frozen_string_literal: true

module WeeklyReportEmail
  extend ActiveSupport::Concern

  included do
    # rubocop:disable Style/FormatString
    def weekly_report_email
      @people_no_title = params[:people_no_title]
      @people_no_dept = params[:people_no_dept]

      begin
        Rails.logger.debug 'EMAIL TO %s' % DulDirectoryAdmin.staff_change_notify_email

        mail(to: DulDirectoryAdmin.staff_change_notify_email,
             subject: 'Weekly Report')
      rescue StandardError => e
        Rails.logger.error '%s' % e
      else
        Rails.logger.info '!!! Staff Weekly Report email sent!'
      end
    end
    # rubocop:enable Style/FormatString
  end
end
