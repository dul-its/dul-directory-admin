# frozen_string_literal: true

class NoticeMailer < ApplicationMailer
  include WeeklyReportEmail

  default from: 'library-system-administration@duke.edu'

  def ldap_updates_email
    @additions = params[:additions]
    @suppressions = params[:suppressions]

    begin
      Rails.logger.debug 'EMAIL TO: %s' % DulDirectoryAdmin.staff_change_notify_email

      mail(to: DulDirectoryAdmin.staff_change_notify_email,
           subject: 'Staff Directory Changes (Additions, Suppressions)')
    rescue StandardError => e
      Rails.logger.error 'ERROR: %s' % e
    else
      Rails.logger.info '!!! Staff Updates Email sent!'
    end
  end

  # send email when a staff member's preferred title has changed
  def email_preferred_title_changed
    @slug = params[:slug]
    @net_id = params[:net_id]
    @new_preferred_title = params[:new_preferred_title]
    @old_preferred_title = params[:old_preferred_title]
    @display_name = params[:display_name]

    begin
      Rails.logger.debug 'EMAIL TO: %s' % DulDirectoryAdmin.staff_change_notify_email

      mail(to: DulDirectoryAdmin.staff_change_notify_email,
           subject: format('Employee Preferred Title Changed: %s (%s)', @display_name, @net_id))
    rescue StandardError => e
      Rails.logger.error 'ERROR: %s' % e
    else
      Rails.logger.info format('!!! Staff Preferred Title email sent (%s)', @net_id)
    end
  end

  # send this mail when it's detected that an employee has been
  # removed from the LDAP feed
  def email_employee_removed
    @netid = params[:netid]
    @display_name = params[:display_name]
    Rails.logger.info '*** EMAIL for REMOVED STAFF ***'
    begin
      mail(to: DulDirectoryAdmin.staff_change_notify_email,
           subject: format('Employee visibility changed: %s (%s)', @display_name, @netid))
    rescue StandardError => e
      Rails.logger.error '%s' % e
    else
      Rails.logger.info format('!!! Staff Visibility Suppression email sent (%s)', @netid)
    end
  end

  # send this mail when it's detected that an employee has been
  # added to the Perkins staff tree
  def email_employee_added
    @netid = params[:netid]
    @display_name = params[:display_name]
    begin
      mail(to: DulDirectoryAdmin.staff_change_notify_email,
           subject: format('Employee added: %s (%s)', @display_name, @netid))
    rescue StandardError => e
      Rails.logger.error '%s' % e
    else
      Rails.logger.info format('!!! Added Staff (%s) Notification email sent', @netid)
    end
  end

  # send this email when a staff member's photo has been updated.
  def email_staff_photo_updated
    @slug = params[:slug]
    @net_id = params[:net_id]
    @display_name = params[:display_name]

    begin
      mail(to: DulDirectoryAdmin.staff_change_notify_email,
           subject: format('Staff Directory photo changed: %s (%s)', @display_name, @net_id))
    rescue StandardError => e
      Rails.logger.error '%s' % e
    else
      Rails.logger.info format('!!! Staff Directory Photo Update (%s) Notification email sent', @net_id)
    end
  end
end
