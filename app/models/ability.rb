# frozen_string_literal: true

class Ability
  include CanCan::Ability

  attr_reader :user

  def initialize(user)
    @user = user || User.new

    # common abilities
    can :access, :rails_admin
    can :read, :dashboard

    # aliases
    alias_action :export, to: :read

    case user.role
    when 'admin'
      can :manage, :all
      cannot :create, Person
      cannot :destroy, Person
    when 'hr_admin'
      can :read, :all
      can :update, Person
      can :manage, [Department, Training]
    when 'subject_manager'
      can :read, :all
      can :update, Person
      can :manage, [Expertise, ExternalContact, SubjectArea]
      can :create, [Expertise, ExternalContact, SubjectArea]
    when 'staff'
      # staff need read access the Language and
      # Training in order to update these on their
      # own Person record.
      can :read, [Language, SubjectArea, Training]
      # staff can only read and update their own Person record
      # IF their person record has `active` (alias of `viewable`)
      # set to TRUE (default is TRUE)
      can :read, Person, net_id: user.net_id, active: true
      can :update, Person, net_id: user.net_id, active: true
    end
  end
end
