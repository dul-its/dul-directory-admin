# frozen_string_literal: true

class Autosuggest
  class << self
    def suggestions
      person_names.concat(department_names)
    end

    private

    def person_names
      Person.is_active.is_valid.order(:display_name).map do |person|
        nickname = person.dul_nickname.present? ? "{#{person.dul_nickname}}" : ''
        "#{person.display_name} #{nickname}"
      end
    end

    def department_names
      Department.order(:name).map(&:name)
    end
  end
end
