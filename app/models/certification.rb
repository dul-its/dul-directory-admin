# frozen_string_literal: true

class Certification < ApplicationRecord
  belongs_to :person, optional: true
  belongs_to :training, optional: true
end
