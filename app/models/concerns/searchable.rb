# frozen_string_literal: true

module Searchable
  extend ActiveSupport::Concern

  included do
    def self.search(query)
      return unless query

      sanitized_query = sanitize_query(query)

      exact_search(sanitized_query) || like_search(sanitized_query)
    end

    def self.sanitize_query(query)
      query.gsub(%r{[^-.&,A-Za-z0-9()' /]}, '')
    end
  end
end
