# frozen_string_literal: true

class Department < ApplicationRecord
  include Searchable

  has_many :roles, dependent: :destroy
  has_many :people, through: :roles

  belongs_to :head_person,
             class_name: 'Person',
             foreign_key: 'head_person_id',
             optional: true
  belongs_to :asst_head_person,
             class_name: 'Person',
             foreign_key: 'asst_head_person_id',
             optional: true
  belongs_to :manually_included_person,
             class_name: 'Person',
             foreign_key: 'manually_included_person_id',
             optional: true
  has_ancestry

  class << self
    def exact_search(query)
      results = where(name: query)
                .order(name: :asc)
      results.count.positive? ? results : nil
    end

    def like_search(query)
      results = where(['name LIKE :query', { query: "%#{query}%" }])
                .order(name: :asc)
      results.count.positive? ? results : nil
    end
  end

  def all_staff_for_display
    [[head_person].reject(&:blank?),
     [asst_head_person].reject(&:blank?),
     [manually_included_person].reject(&:blank?),
     active_children_dept_heads,
     active_children_asst_dept_heads,
     active_descendant_dept_heads,
     active_descendant_asst_dept_heads,
     active_staff].compact
      .reduce(&:concat)
      .uniq
  end

  def to_param
    return nil unless persisted?

    slug
  end

  private

  def active_staff
    [people, descendants.map(&:people)].flatten
                                       .compact
                                       .select(&:active?)
                                       .reject(&:dept_head?)
                                       .reject(&:asst_dept_head?)
                                       .uniq
                                       .sort_by { |p| [p.last_name] }
  end

  def active_children_dept_heads
    children.sort_by(&:name)
            .map(&:head_person)
            .uniq
            .compact
            .select(&:active?)
  end

  def active_children_asst_dept_heads
    children.sort_by(&:name)
            .map(&:asst_head_person)
            .uniq
            .compact
            .select(&:active?)
  end

  def active_descendant_dept_heads
    descendants.reject { |c| c.depth == 2 }
               .sort_by { |d| [d.depth, d.parent.name, d.name] }
               .map(&:head_person)
               .uniq
               .compact
               .select(&:active?)
  end

  def active_descendant_asst_dept_heads
    descendants.reject { |c| c.depth == 2 }
               .sort_by { |d| [d.depth, d.parent.name, d.name] }
               .map(&:asst_head_person)
               .uniq
               .compact
               .select(&:active?)
  end
end
