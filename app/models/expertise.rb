# frozen_string_literal: true

class Expertise < ApplicationRecord
  belongs_to :person, optional: true
  belongs_to :subject_area, optional: true
end
