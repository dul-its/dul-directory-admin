# frozen_string_literal: true

class ExternalContact < ApplicationRecord
  has_many :external_expertise, dependent: :destroy
  has_many :subject_areas, through: :external_expertise
end
