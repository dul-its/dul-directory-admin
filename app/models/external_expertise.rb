# frozen_string_literal: true

class ExternalExpertise < ApplicationRecord
  belongs_to :external_contact, optional: true
  belongs_to :subject_area, optional: true
end
