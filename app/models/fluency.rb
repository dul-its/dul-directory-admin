# frozen_string_literal: true

class Fluency < ApplicationRecord
  belongs_to :person
  belongs_to :language
end
