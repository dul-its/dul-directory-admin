# frozen_string_literal: true

class Language < ApplicationRecord
  has_many :fluencies, dependent: :destroy
  has_many :people, through: :fluencies
end
