# frozen_string_literal: true

# rubocop:disable Metrics/ClassLength
class Person < ApplicationRecord
  include Searchable

  has_many :roles, dependent: :destroy
  has_many :departments, through: :roles

  has_many :certifications, dependent: :destroy
  has_many :trainings, through: :certifications

  has_many :fluencies, dependent: :destroy
  has_many :languages, through: :fluencies

  has_many :expertise, dependent: :destroy
  has_many :subject_areas, through: :expertise

  has_one_attached :staff_photo
  attr_accessor :remove_staff_photo

  after_save do
    if staff_photo.attached?
      staff_photo.blob.update(filename: "#{first_name}_#{last_name}_staff_photo" \
                                        ".#{staff_photo.filename.extension}".downcase
                                                                            .gsub(/[^a-z_.]/, ''))
    end
  end
  after_save { staff_photo.purge if remove_staff_photo == '1' }

  after_update :notify_hr_of_staff_photo_update, :notify_hr_of_preferred_title_update

  validates :staff_photo, content_type: ['image/png', 'image/jpg', 'image/jpeg'],
                          dimension: { width: { min: 600 },
                                       height: { min: 800 } }

  alias_attribute :active, :viewable

  scope :is_active, -> { where(active: true) }

  class << self
    def exact_search(query)
      results = is_active
                .where(display_name: query)
                .order(display_name: :asc)
      results.count.positive? ? results : nil
    end

    def like_search(query)
      results = is_active
                .where(['first_name LIKE :query OR ' \
                        'last_name LIKE :query OR ' \
                        'display_name LIKE :query',
                        { query: "%#{query}%" }])
                .order(display_name: :asc)
                .order(first_name: :asc)
                .order(last_name: :asc)
      results.count.positive? ? results : nil
    end

    # rubocop:disable Naming/PredicateName
    def is_valid
      where.not(display_name: nil)
           .where.not(first_name: nil)
           .where.not(last_name: nil)
           .where('slug REGEXP ?', '[A-Za-z]+')
    end
    # rubocop:enable Naming/PredicateName
  end

  # for rails admin display
  def custom_label_method
    "#{last_name}, #{first_name}"
  end

  def display_title
    return preferred_title if preferred_title.present?

    title
  end

  def eg_member?
    is_eg_member == true
  end

  def head_of_library?
    eg_member? && departments.map { |d| d.name == 'Administration' }.any?
  end

  def dept_head?
    departments.map(&:head_person).include?(self)
  end

  def asst_dept_head?
    departments.map(&:asst_head_person).include?(self)
  end

  def to_param
    return nil unless persisted?

    slug
  end

  private

  def notify_hr_of_staff_photo_update
    return if staff_photo.attachment.nil?
    return if staff_photo.attachment.created_at.nil?
    return unless staff_photo.attached? && (Time.now - staff_photo.attachment.created_at) < 5

    begin
      NoticeMailer.with(slug: slug, net_id: net_id, display_name: display_name)
                  .email_staff_photo_updated
                  .deliver_now
    rescue ArgumentError => e
      Rails.logger.error e
    ensure
      Rails.logger.info("Staff Photo Updated (Slug: #{slug}; Net ID: #{net_id}; Display Name: #{display_name})")
    end
  end

  def notify_hr_of_preferred_title_update
    return if preferred_title_previously_was.nil?
    return if preferred_title == preferred_title_previously_was

    begin
      NoticeMailer.with(slug: slug,
                        net_id: net_id,
                        display_name: display_name,
                        new_preferred_title: preferred_title,
                        old_preferred_title: preferred_title_previously_was)
                  .email_preferred_title_changed
                  .deliver_now
    rescue ArgumentError => e
      Rails.logger.error e
    ensure
      Rails.logger.info("Staff Title Updated (Slug: #{slug}; Net ID: #{net_id}; Display Name: #{display_name})")
    end
  end
end
# rubocop: enable Metrics/ClassLength
