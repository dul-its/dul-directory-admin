# frozen_string_literal: true

class Role < ApplicationRecord
  belongs_to :department
  belongs_to :person
end
