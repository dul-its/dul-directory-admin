# frozen_string_literal: true

class SubjectArea < ApplicationRecord
  has_many :expertise, dependent: :delete_all
  has_many :people, through: :expertise

  has_many :external_expertise, dependent: :delete_all
  has_many :external_contacts, through: :external_expertise

  validates :title, presence: true
end
