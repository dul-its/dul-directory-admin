# frozen_string_literal: true

class Training < ApplicationRecord
  has_many :certifications, dependent: :delete_all
  has_many :people, through: :certifications

  validates :title, presence: true
end
