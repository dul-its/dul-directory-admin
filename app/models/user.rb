# frozen_string_literal: true

class User < ApplicationRecord
  validates :username, presence: true, uniqueness: true
  devise :database_authenticatable, :omniauthable, omniauth_providers: [:shibboleth]
  before_create :set_generated_password, if: :password_not_set?
  enum role: { staff: 0, hr_admin: 1, admin: 2, subject_manager: 3 }

  def self.from_omniauth(auth)
    user = find_by(username: auth.uid) || new(username: auth.uid)
    user.update!(email: auth.info.email)

    user
  end

  def net_id
    username.gsub('@duke.edu', '')
  end

  def admin?
    role == 'admin'
  end

  def hr_admin?
    role == 'hr_admin'
  end

  def subject_manager?
    role == 'subject_manager'
  end

  def staff?
    role == 'staff'
  end

  def uid
    'uid'
  end

  def self.generate_password
    Devise.friendly_token
  end

  private

  def set_generated_password
    self.password = User.generate_password
  end

  def password_not_set?
    password.nil?
  end
end
