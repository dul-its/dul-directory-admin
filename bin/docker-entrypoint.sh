#!/bin/sh
# https://stackoverflow.com/a/38732187/1935918
set -e

if [ -f /app/tmp/pids/server.pid ]; then
  rm /app/tmp/pids/server.pid
fi

wait-for-it db:3306 -t 60 --

if [ "$RAILS_ENV" != "production" ]; then
  yarn install

  bin/rails db:drop  2>/dev/null

  bin/rails db:create 2>/dev/null

  bin/rails db:migrate 

  bin/rails db:seed

  bin/rails staff_photo:attach
  
  bin/rails user:create:development_users
fi

if [ "$RAILS_ENV" = "production" ]; then
  bin/rails db:migrate 2>/dev/null
fi

exec bundle exec "$@"
