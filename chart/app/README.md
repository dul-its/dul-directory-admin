# Helm-based Deployment

## Prerequisites

### Use OpenShift CLI
**BEFORE PROCEEDING FURTHER**, make sure you:
* have installed `oc`, `helm`, *AND*
* have connected to the OKD instance.
  
***Need a refresher?***  
[Installing (and Using) OpenShift CLI and Helm](https://gitlab.oit.duke.edu/devops/antsy/-/wikis/Installing-OpenShift-and-Helm)
  
Now, you're ready to proceed.

## TL;DR

### Reinstall Staff Directory
```bash
$ cd /path/to/dul-directory-admin/staff-directory

$ oc project dul-directory-prod # production
# oc project dul-directory-stage for staging

# production
$ helm upgrade --install production \
    --values values-prod.yaml \
    --set proxy.entityID=directory.library.duke.edu \
    --set-file tls.crt=../sp-prod.pem,tls.key=../server-prod.key \
    --set-file proxy.shibboleth.attributeMap=attribute-map.xml \
    --set image.tag=[LATEST_TAGGED_RELEASE_FROM_GITLAB] \
    .
# locate the latest tagged 2.x.x release in this project's repo
# https://gitlab.oit.duke.edu/dul-its/dul-directory-admin/-/tags
#
# or
# git tag -l --sort=-v:refname
# (ignore any non 2.x.x in the output)

# ----

# or stage
$ helm upgrade --install stage \
    --values values-stage.yaml \
    --set proxy.entityID=directory-stage.library.duke.edu \
    --set-file tls.crt=../sp-stage.pem,tls.key=../server-stage.key \
    --set-file proxy.shibboleth.attributeMap=attribute-map.xml \
    --set image.tag=latest \
    .

# then verify the cron jobs
$ oc get cronjobs
# the output should look like this...
NAME                                SCHEDULE     SUSPEND   ACTIVE    LAST SCHEDULE   AGE
production-staff-directory-daily    30 6 * * *   False     0         11h             26d
production-staff-directory-weekly   00 7 * * 1   False     0         10h             26d

### verify the [RELEASE.NAME]-staff-directory-weekly job's schedule is '00 7 * * 1' instead of 
### '* 7 * * 1'.
### --
### In the event the schedule is incorrect, run this:
### $ kubectl patch cronjob [RELEASE.NAME]-staff-directory-weekly -p '{"spec":{"schedule": "00 7 * * 1"}}'
### (where RELEASE.NAME is one of "production" or "stage"
```

 
## "From-Scratch" Setup
FOR DOCUMENTATION PURPOSES  
  
This will likely not be necessary once the application is 
running.  
  
```
cd /path/to/dul-directory/staff-directory
```  
`staff-directory` is the directory that contains our Helm chart.  
  
## Create Database Secret
```sh
kubectl create secret generic staff-directory-mariadb \
  --from-literal=mariadb-root-password=<redacted> \
  --from-literal=mariadb-password=<redacted>
```

## Create TLS Secret
While the templates (under `/templates`) take care of most aspects of deploying an application instance, 
you'll need to create a "TLS Secret" that contains an  certificate and private key.  
  
```sh
kubectl create secret tls $(oc project -q)-tls \
  --cert=/path/to/cert \
  --key=/path/to/key \
  --certificate-authority=/path/to/ca-cert
```
## Shibboleth/Reverse Proxy Setup
Unlike `spacefinder`, our stack for `staff-directory` includes a deployment that will run 
Shibboleth and HTTPD side-by-side with our Rails app.  
  
### Set Up Your Shibboleth Provider (SP)
Let's assume we're using `directory-stage.library.duke.edu` as our hostname.  
  
We'll need to create a record in OIT's [Authentication service](https://authentication.oit.duke.edu/manager), where:
* we'll set the Entity ID = `https://directory-stage.library.duke.edu`,
* we'll release the following attributes
  - eduPersonPrincipalName
  - eduPersonScopedAffiliation
  - uid
  - mail
  - displayName
  - cn
* we'll release the following group: `urn:mace:duke.edu:groups:library:perkins`
  
We'll also need to copy our certificate into the registration form.  
  

## Values Overrides
When installing our helm chart, the values in `values.yaml` will be used to set up the instance -- along with the templates.  
  
For specific settings for a particular instance (dev, pre, prod, etc), you can create override values. For instance: 
```yaml
ingress:
  enabled: true
  className: ""
  annotations: {}
  hosts:
    - host: directory-stage.library.duke.edu
      paths:
        - path: /
          pathType: ImplementationSpecific
  tls:
    - secretName: dul-directory-stage-tls
      hosts:
        - directory-stage.library.duke.edu

```
  
**Note** - Your desired host needs to be a CNAME that points to `os-node-lb-fitz.oit.duke.edu`.

## Install The Chart
Let's install our app using the helm chart...  
  
```bash
$ helm upgrade --install --values values-stage.yaml \
  --set proxy.entityID=directory-stage.library.duke.edu \
  --set-file tls.crt=../sp-stage.pem \
  --set-file tls.key=../server-stage.key \
  --set-file proxy.shibboleth.attributeMap=attribute-map.xml \
  stage .
```

