#config/initializers/dul_ldap.rb
require "dul_ldap"

# no defaults are provided for these 2 settings.
# developers and admins will need to be aware of the absence 
# of these environment variables
Rails.application.config.x.dul_ldap.binddn = ENV.fetch("DUL_LDAP_BINDDN")
Rails.application.config.x.dul_ldap.bindpw = ENV.fetch("DUL_LDAP_BINDPW")

# load in the target URN, base (DN -- for searching), and 
# attributes to be included for each LDAP entry (search results).
DulLdap.config = YAML.load_file("config/dul_ldap.yml")[Rails.env].symbolize_keys

