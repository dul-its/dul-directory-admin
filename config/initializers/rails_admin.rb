RailsAdmin.config do |config|

  def department_fields_info
    [
      :name,
      :slug,
      :description,
      :url,
    ]
  end

  def department_fields_members
    [
      :people,
      :head_person,
      :asst_head_person,
      :manually_included_person,
    ]
  end

  def department_fields_contact
    [
      :campus_box,
      :physical_address,
      :email,
      :phone,
      :phone2,
      :fax,
      :contact_form_url,
      :contact_form_url_label_text,
    ]
  end

  def department_fields_other
    [
      :ancestry,
      :position,
      :head_title,
      :sap_org_unit,
    ]
  end

  def person_fields_from_sap
    [
      :first_name,
      :middle_name,
      :last_name,
      :nickname,
      :display_name,
      :pronouns,
      :net_id,
      :unique_id,
      :title,
      :sap_org_unit,
      :primary_affiliation,
      :affiliation,
      :email,
      :email_privacy,
      :phone,
      :ldap_dn
    ]
  end

  def person_fields_self_managed
    [
      :preferred_title,
      :profile,
      :languages,
      :trainings,
      :staff_photo,
      :orcid,
      :libguide_id,
      :libcal_id,
      :campus_box,
      :physical_address,
      :dul_nickname,
      :hide_phone_number
    ]
  end

  def person_fields_subject_manager_only
    [
      :subject_areas
    ]
  end

  def person_fields_library_hr_only
    [
      :slug,
      :viewable,
      :departments,
      :is_eg_member
    ]
  end

  config.parent_controller = 'ApplicationController'

  config.asset_source = :sprockets

  ## == Devise ==
  config.authenticate_with do
    warden.authenticate! scope: :user
  end
  config.current_user_method(&:current_user)

  ## == Cancan ==
  config.authorize_with :cancancan


  config.main_app_name = ["DUL Staff Directory", "BackOffice"]

  config.actions do
    dashboard                     # mandatory
    index                         # mandatory
    new
    export
    bulk_delete
    show
    edit
    delete
    show_in_app

    ## With an audit adapter, you can add:
    # history_index
    # history_show

    # Add the nestable action for configured models
    nestable

  end

  config.included_models = %w[ Department ExternalContact Language Person SubjectArea Training User ]

  # make Person names display instead of just an ID (defined in Person model)
  config.model 'Person' do
    configure :roles do
      visible(false)
    end
    configure :certifications do
      visible(false)
    end
    configure :fluencies do
      visible(false)
    end
    configure :expertise do
      visible(false)
    end
    object_label_method do
      :custom_label_method
    end
    configure :photo_id do
      read_only true
      visible(false)
    end
    configure :photo_url do
      visible(false)
    end
    configure :libguide_id do
      html_attributes size: 10
    end
    configure :libcal_id do
      html_attributes size: 10
    end

    edit do

    # staff-managed fields 
      group :self_managed_field_group do
        label "Self-Managed Info"
        help "Staff can manage the information in their staff directory \
          entry by updating the fields below."
      end
      person_fields_self_managed.each do |f|
        if f == :profile
          field :profile, :ck_editor do
            config_js ActionController::Base.helpers.asset_path('ckeditor/config.js')
            group :self_managed_field_group
          end
        elsif f == :campus_box
          field :campus_box, :text do
            html_attributes rows: 5, cols: 100
            group :self_managed_field_group
          end
        elsif f == :physical_address
          field :physical_address, :text do
            html_attributes rows: 5, cols: 100
            group :self_managed_field_group
          end
        elsif f == :dul_nickname
          field :dul_nickname, :string do
            label "Nickname"
            help "Optional &mdash; used only for searching; e.g. 'Joe' to find Joseph".html_safe
            group :self_managed_field_group
          end
        elsif f == :hide_phone_number
          field :hide_phone_number, :boolean do
            label "Hide my phone number"
            group :self_managed_field_group
          end
        else
          field f do
            group :self_managed_field_group
          end
        end
      end

    # Subject Specialists
      group :subject_manager_field_group do
        label "Subject Areas"
        help "These are assigned by Subject Managers. \
          Staff cannot manage these themselves."
        active true
      end
      person_fields_subject_manager_only.each do |f|
        field f do
          group :subject_manager_field_group
          read_only do
            !bindings[:view].current_user.subject_manager? && !bindings[:view].current_user.admin?
          end
        end
      end
      
    # Duke HR info 
      group :sap_field_group do
        label "Info from Duke HR"
        help "This info comes from SAP so it can't be edited here. \
          Some fields (as marked) can be self-managed via \
          <a href='https://work.duke.edu/irj/portal/MyInfo'>Duke@Work</a> \
          or <a href='https://idms-web.oit.duke.edu/portal/'>Duke OIT \
          Account Self-Service</a>".html_safe
      end
      person_fields_from_sap.each do |f|
        field f do
          group :sap_field_group
          read_only true
        end
      end
      include_all_fields

    # Library HR-managed fields 
      group :lhr_field_group do
        label "Info Managed by Library HR"
        help "This info can only be edited by Library HR. \
          Staff cannot edit it themselves."
        active false
      end
      person_fields_library_hr_only.each do |f|
        field f do
          group :lhr_field_group
          read_only do
            !bindings[:view].current_user.hr_admin? && !bindings[:view].current_user.admin?
          end
        end
      end

    end
  end

  # make Departments nestable
  config.model 'Department' do
    configure :roles do
      visible(false)
    end
    nestable_tree({
      position_field: :position,
      max_depth: 6
    })
    edit do
      group :department_fields_info_group do
        label "Basic Info"
      end
      department_fields_info.each do |f|
        if f == :description
          field :description, :ck_editor do
            config_js ActionController::Base.helpers.asset_path('ckeditor/config.js')
            # Note: ckeditor ignores the maxlength attribute
            html_attributes do
              { maxlength: 1000, rows: 8, cols: 60 }
            end
            group :department_fields_info_group
          end
        else
          field f do
            group :department_fields_info_group
          end
        end
      end
      group :department_fields_members_group do
        label "People in This Department"
        help "Do not include staff who are members of sub-departments."
      end
      department_fields_members.each do |f|
        field f do
          group :department_fields_members_group
        end
      end

      group :department_fields_contact_group do
        label "Contact Info"
      end
      department_fields_contact.each do |f|
        field f do
          group :department_fields_contact_group
        end
      end
      field :contact_form_url do
        label "Contact Form URL"
        help "Optional &mdash; include link to departmental contact form".html_safe
      end
      field :contact_form_url_label_text do
        label "Contact Form Label Text"
        help "Optional &mdash; the text you'd like to use for the contact form link above".html_safe
      end

      group :department_fields_other_group do
        label "Other Info"
        help "Set this department's position in the org chart hierarchy via the \
          <a href='/admin/department/nestable'>Department Sorting</a> tab.".html_safe
        active false
      end
      department_fields_other.each do |f|
        field f do
          group :department_fields_other_group
        end
      end

      include_all_fields
    end
  end

  # hide certifications in trainings
  config.model 'Training' do
    configure :certifications do
      visible(false)
    end
  end

  # hide expertise in external contacts
  config.model 'ExternalContact' do
    configure :external_expertise do
      visible(false)
    end
  end

  # hide expertise in subject areas
  config.model 'SubjectArea' do
    configure :expertise do
      visible(false)
    end
    configure :external_expertise do
      visible(false)
    end
  end

  # hide lookup tables
  config.model 'Certification' do
    visible false
  end
  config.model 'Role' do
    visible false
  end
  config.model 'Expertise' do
    visible false
  end
  config.model 'ExternalExpertise' do
    visible false
  end
  config.model 'Fluency' do
    visible false
  end

  config.model 'ActiveStorage::Attachment' do
    visible false
  end

  config.model 'ActiveStorage::Blob' do
    visible false
  end

  config.model 'ActiveStorage::VariantRecord' do
    visible false
  end

end