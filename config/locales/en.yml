# Files in the config/locales directory are used for internationalization
# and are automatically loaded by Rails. If you want to use locales other
# than English, add the necessary files in this directory.
#
# To use the locales, use `I18n.t`:
#
#     I18n.t 'hello'
#
# In views, this is aliased to just `t`:
#
#     <%= t('hello') %>
#
# To use a different locale, set it with `I18n.locale`:
#
#     I18n.locale = :es
#
# This would use the information in config/locales/es.yml.
#
# The following keys must be escaped otherwise they will not be retrieved by
# the default I18n backend:
#
# true, false, on, off, yes, no
#
# Instead, surround them with single quotes.
#
# en:
#   'true': 'foo'
#
# To learn more, please read the Rails Internationalization guide
# available at http://guides.rubyonrails.org/i18n.html.

en:
  activerecord:
    attributes:
      person:
        is_eg_member: 'EG Member?'
        ldap_dn: 'LDAP DN'
        libguide_id: 'Libguides Account ID'
        libcal_id: 'Libcal Account ID'
        net_id: 'NetID'
        pronouns: 'Personal Pronouns'
        sap_org_unit: 'SAP Org Unit'
        unique_id: 'Duke Unique ID'
        department:
        ancestry: 'Ancestor Dept IDs'
        asst_head_person: 'Asst. Head'
        head_person: 'Head'
        orcid: 'ORCID'
        position: 'Sort Position'
        sap_org_unit: 'SAP Org Unit'
        viewable: Active?
  admin:
    navbar:
      greeting: 'Hello'
    dashboard:
      edit_yourself: 'Edit Your Profile'
    help:
      person:
        affiliation: ''
        campus_box: |
          Add your preferred campus mailing address here (if left blank, will 
          not display on your directory page)
        departments: 'Select dept(s) & click right arrow to activate.'
        display_name: |
          Edit via <a href="https://idms-web.oit.duke.edu/portal/">Duke OIT Account Self-Service
          </a>: <ol><li>Click <strong>Manage Directory Listings</strong></li><li>Click <strong>
          Change your name</strong></li><li>Click <strong>Update Display Name</strong> button</li>
        email_privacy: |
          Edit via <a href="https://idms-web.oit.duke.edu/portal/">Duke OIT Account Self-Service
          </a>: <ol><li>Click <strong>Manage Directory Listings</strong></li><li>Click <strong>Set
          who can see your email address</strong></li>
        email: |
          Edit via <a href="https://idms-web.oit.duke.edu/portal/">Duke OIT Account Self-Service
          </a>: <ol><li>Click <strong>Manage Directory Listings</strong></li><li>Click <strong>
          Change your preferred email address</strong></li>
        first_name: ''
        is_eg_member: ''
        languages: 'Select language(s) & click right arrow to activate.'
        last_name: ''
        ldap_dn: ''
        libguide_id: |
          You can find this in the link that lists all of your libguides:<br />
          https://guides.library.duke.edu/prf.php?account_id=12345<br />(in this example the ID is
          12345)
        libcal_id: 'Add your ID here to enable appointments via libcal'
        middle_name: ''
        net_id: ''
        nickname: ''
        orcid: 'More information on <a href="https://scholarworks.duke.edu/orcid/">ORCID</a>'
        photo_id: ''
        phone: |
          Edit via <a href="https://work.duke.edu/irj/portal/MyInfo">Duke@Work MyInfo</a> page;
          once there follow these steps:<ol><li>Click <strong>My Profile</strong></li><li>Click
          <strong>Maintain your Addresses & Phone Numbers</strong></li><li>Under <strong>Duke
          External Mailing Address</strong>, click <strong>Edit</strong><li>Edit the <strong>
          Telephone</strong> field</li></ol>
        photo_url: ''
        physical_address: |
          Add your preferred physical location here (if left blank, will not display on your 
          directory page)
        preferred_title: |
          Preferred job title. If left blank, the default HR value will be displayed.
        primary_affiliation: ''
        profile: |
          Brief description of role, activities, and/or background. If this is blank but a <a
          href="https://scholars.duke.edu/">Scholars@Duke</a> profile has text, that will display
          in the directory page.
        pronouns: |
          Edit via <a href="https://work.duke.edu/irj/portal/MyInfo">Duke@Work MyInfo</a> page;
          once there follow these steps: <ol><li>Click <strong>My Profile</strong></li><li>Click 
          the link for <strong>Select/Update my Gender Identity, Pronouns, and Preferred First Name
          </strong></li><li>Make sure to also set your pronouns to be public by visiting 
          <a href="https://idms-web-selfservice.oit.duke.edu/">Identity Manager</a> then clicking on 
          <strong>Manage Name and Pronoun Settings</strong>, clicking <strong>Edit</strong>, and 
          setting <strong>Pronoun Privacy</strong> to 'Provide my pronouns...'</li></ol>
        sap_org_unit: ''
        slug: 'Appears in the URL for the person in the directory.'
        staff_photo: |
          Upload your staff profile image. Images should be in portrait orientation and at least
          600x800 pixels.
        subject_areas: |
          This section is for use by Subject Librarians only; select one or more subject areas
          from the list above and click the right arrow to activate. If you have selected a
          subject area, your name and contact information will be included on DUL's
          <a href="/subject-specialists">Subject Specialists</a> page.
        title: ''
        trainings: 'Select training(s) & click right arrow to activate.'
        unique_id: ''
        viewable: |
          Display this person in the public staff directory and allow this person to edit their own
          directory entry?
      department:
        ancestry: |
          Edit via the drag & drop <a href="/admin/department/nestable">Department Sorting</a> tab.
        asst_head_person: ''
        campus_box: ''
        description: 'Brief description of the department. Please limit to 1,000 characters.'
        email: ''
        fax: ''
        head_person: ''
        head_title: 'Not presently used by the directory UI.'
        name: ''
        people: ''
        phone: ''
        phone2: ''
        physical_address: ''
        position: |
          Edit via the drag & drop <a href="/admin/department/nestable">Department Sorting</a> tab.
        sap_org_unit: 'Not presently used by the directory UI.'
        slug: 'Appears in the URL for the department in the directory.'
        url: 'URL for departmental website.'
    flash:
      successful: "%{name} successfully %{action}."
  errors:
    messages:
      content_type_invalid: "must be a JPG or PNG file"
  public_ui:
    skip_to_content: 'Skip to main content'
    breadcrumbs:
      about: 'About'
      about_path: '/about'
      home: 'Home'
      directory_home_path: '/'
    title: 'Duke University Libraries Staff Directory'
    short_title: 'Staff Directory'
    organization:
      name: 'Duke University Libraries'
      url: 'https://library.duke.edu'
    browse:
      all: 'ALL'
      aria_label: 'Browse staff by letter'
      title: "Browse Staff: %{letter}"
    dept:
      campus_box: 'Campus Box'
      fax: '(fax)'
      title: Departments
      search: 'Filter by Department Name'
      search_results:
        title: Departments Matching Your Search
      contact_label: 'Department Contact Information'
      mailing_address_label: 'Department Mailing Address'
      mailing_address_line2: 'Durham, NC 27708'
      location: 'Department Location'
    dept_nav:
      aria_label: "Departments"
      ex_group_title: 'Executive Group'
      org_chart_title: 'Organizational Chart'
    executive_group:
      title: 'Library Executive Group'
    footer:
      login: 'Login to Staff Directory'
    navbar:
      aria_label: 'Staff Directory'
      active_sr_only_html: '<span class="sr-only">(current)</span>'
      search:
        action: 'Submit'
        aria_label: 'Search staff by name'
        label: 'Search by name:'
        placeholder: 'Search by name...'
      title: 'Directory Navigation'
      toggle: 'Toggle Directory Navigation'
    person:
      edit_profile: 'Edit Profile'
      orcid_title: 'ORCID profile'
      scholars_at_duke_title: 'Scholars@Duke Profile'
      department_header:
        one: 'Department'
        other: 'Departments'
      language_skill_header:
        one: 'Language Skill'
        other: 'Language Skills'
      subject_area_header:
        one: 'Subject Area'
        other: 'Subject Areas'
      training_header:
        one: 'Training & Certification'
        other: 'Trainings & Certifications'
      research_guide_header: 'Top Research Guides'
      view_all_guides_html: 'View all guides &raquo;'
      view_research_guides: 'View Research Guides'
      schedule_an_appointment: 'Schedule an Appointment'
      pronouns: 'Pronouns'
      contact_label: 'Contact Information'
      mailing_address: 'Mailing Address'
      location: 'Location'
      toggle_profile: 'Toggle full profile view'
    root:
      title: 'Staff Directory'
    search:
      departments_heading: "Departments Matching Your Search"
      no_query: 'Please enter a search term'
      no_results_html: "No results found for <em>%{query}</em>"
      staff_heading: 'Staff Matching Your Search'
      title: 'Search Results'
      view_all_departments_html: 'Browse All Departments &raquo;'
      view_all_people_html: 'All Staff A-Z &raquo;'
    subject_specialists:
      title: 'Subject Specialists'
      search: 'Filter by Subject or Name'
    staff_a_to_z:
      title: 'Staff A–Z'
      sr_title: 'Staff A, to Z'
