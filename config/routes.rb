# frozen_string_literal: true

module ActionDispatch
  module Routing
    class Mapper
      def draw(routes_name)
        routes_path = Rails.root.join('config', 'routes', (@scope[:shallow_prefix]).to_s, "#{routes_name}.rb")

        instance_eval(File.read(routes_path))
     end
    end
  end
end

# point to files in config/routes
Rails.application.routes.draw do
  draw :app
  draw :api
  draw :redirects
  draw :models
  draw :exceptions
end
