# frozen_string_literal: true

# get 'api/show'

# need this route to facilitate staff directory refresh from Duke LDAP
match 'api/refresh_from_ldap' => 'api#refresh_from_ldap', :via => :get
match 'api/do_weekly_report' => 'api#do_weekly_report', :via => :get
match 'api/check_membership' => 'api#check_membership', :via => :get
