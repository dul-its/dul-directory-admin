# frozen_string_literal: true

default_url_options :host => 'localhost:3000'

# devise_for :users, controllers: { sessions: 'users/sessions' }
devise_for :users, controllers: { omniauth_callbacks: "users/omniauth_callbacks" }

# Logout via GET
devise_scope :user do
  get '/users/sign_out(.:format)', to: 'devise/sessions#destroy'
end

mount RailsAdmin::Engine => '/admin', as: 'rails_admin'
# For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

# healthcheck
get 'health-check', to: 'health#index'