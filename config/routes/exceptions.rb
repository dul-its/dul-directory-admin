# frozen_string_literal: true

get '/403', to: 'errors#forbidden'
get '/404', to: 'errors#not_found'
get '/422', to: 'errors#rejected'
get '/500', to: 'errors#internal_server'