# frozen_string_literal: true

get 'departments/:slug', to: redirect('/dept/%{slug}')
get 'people/:slug', to: redirect('/staff/%{slug}'), slug: %r{[^/]+}

resources :people, param: :slug
resources :departments, param: :slug

root to: 'browse#index', as: 'browse_index'

get '/browse', to: redirect('/')
get '/browse/all', to: redirect('/')
get '/browse/:id', to: 'browse#show', as: 'browse_show'

get '/dept', to: 'departments#index', as: 'dept_index'
get '/dept/:slug', to: 'departments#show', as: 'dept_show'

get '/executive-group', to: 'executive_group#index', as: 'executive_group_index'

get '/staff', to: redirect('/')
get '/staff/:slug', to: 'people#show', as: 'staff_show', slug: %r{[^/]+} # permits firstname.lastname

get '/subject-specialists', to: 'subject_specialists#index', as: 'subject_specialists_index'

get '/search', to: 'searches#search', as: 'search'