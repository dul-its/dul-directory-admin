# frozen_string_literal: true

get '/dept/technical-services', to: redirect('/dept/collections-services')