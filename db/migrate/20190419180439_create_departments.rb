class CreateDepartments < ActiveRecord::Migration[5.2]
  def change
    create_table :departments do |t|
      t.string :name
      t.string :description
      t.string :sap_org_unit, limit: 20
      t.integer :head_person_id
      t.string :head_title
      t.boolean :is_admin_unit
      t.string :campus_box
      t.string :physical_address
      t.string :email
      t.string :fax, limit: 40
      t.string :phone, limit: 40
      t.string :phone2, limit: 40
      t.string :url
      t.string :ancestry
      t.integer :position

      t.timestamps
    end
    add_index :departments, [:ancestry]
  end
end
