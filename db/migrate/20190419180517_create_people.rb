class CreatePeople < ActiveRecord::Migration[5.2]
  def change
    create_table :people do |t|
      t.string :first_name, limit: 40
      t.string :middle_name, limit: 40
      t.string :last_name, limit: 40
      t.string :nickname, limit: 40
      t.string :display_name, limit: 80
      t.string :pronouns
      t.string :net_id, limit: 20
      t.string :unique_id, limit: 20
      t.string :title
      t.string :preferred_title
      t.boolean :is_eg_member
      t.text :keywords
      t.string :photo_url, limit: 200
      t.string :ldap_dn
      t.string :sap_org_unit, limit: 20
      t.string :primary_affiliation, limit: 40
      t.string :affiliation, limit: 40
      t.string :campus_box
      t.string :physical_address
      t.string :email
      t.string :fax, limit: 40
      t.string :phone, limit: 40
      t.string :phone2, limit: 40
      t.string :url
      t.boolean :email_privacy
      t.text :profile
      t.text :office_hours
      t.boolean :viewable

      t.timestamps
    end
  end
end
