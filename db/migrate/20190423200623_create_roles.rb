class CreateRoles < ActiveRecord::Migration[5.2]
  def change
    create_table :roles do |t|
      t.integer :department_id
      t.integer :person_id

      t.timestamps
    end

    add_index :roles, [:department_id, :person_id]
  end
end