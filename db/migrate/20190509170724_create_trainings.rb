class CreateTrainings < ActiveRecord::Migration[5.2]
  def change
    create_table :trainings do |t|
      t.string :title, limit: 60
      t.string :description, limit: 255
      t.boolean :active

      t.timestamps
    end
  end
end
