class CreateCertifications < ActiveRecord::Migration[5.2]
  def change
    create_table :certifications do |t|
      t.integer :person_id
      t.integer :training_id

      t.timestamps
    end

    add_index :certifications, [:person_id, :training_id]
  end
end