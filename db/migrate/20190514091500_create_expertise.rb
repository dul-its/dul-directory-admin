class CreateExpertise < ActiveRecord::Migration[5.2]
  def change
    create_table :expertise do |t|
      t.integer :person_id
      t.integer :subject_area_id

      t.timestamps
    end

    add_index :expertise, [:person_id, :subject_area_id]
  end
end