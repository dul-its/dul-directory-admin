class CreateSubjectAreas < ActiveRecord::Migration[5.2]
  def change
    create_table :subject_areas do |t|
      t.string :title, limit: 255

      t.timestamps
    end
  end
end
