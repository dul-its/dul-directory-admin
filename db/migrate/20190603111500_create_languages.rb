class CreateLanguages < ActiveRecord::Migration[5.2]
  def change
    create_table :languages do |t|
      t.string :name, limit: 255

      t.timestamps
    end
  end
end
