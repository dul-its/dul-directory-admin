class CreateFluencies < ActiveRecord::Migration[5.2]
  def change
    create_table :fluencies do |t|
      t.integer :person_id
      t.integer :language_id

      t.timestamps
    end

    add_index :fluencies, [:person_id, :language_id]
  end
end