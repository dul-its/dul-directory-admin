class RemoveOfficeHoursFromPeople < ActiveRecord::Migration[5.2]
  def change
    remove_column :people, :office_hours, :text
  end
end
