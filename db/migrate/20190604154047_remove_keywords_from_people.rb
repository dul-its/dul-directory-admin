class RemoveKeywordsFromPeople < ActiveRecord::Migration[5.2]
  def change
    remove_column :people, :keywords, :text
  end
end
