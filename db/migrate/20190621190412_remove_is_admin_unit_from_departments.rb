class RemoveIsAdminUnitFromDepartments < ActiveRecord::Migration[5.2]
  def change
    remove_column :departments, :is_admin_unit, :boolean
  end
end
