class AddPhotoIdToPeople < ActiveRecord::Migration[5.2]
  def change
    add_column :people, :photo_id, :integer, after: :is_eg_member
  end
end
