class AddSlugToPeople < ActiveRecord::Migration[5.2]
  def change
    add_column :people, :slug, :string
  end
end
