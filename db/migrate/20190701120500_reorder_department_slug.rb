class ReorderDepartmentSlug < ActiveRecord::Migration[5.2]
  def up
    change_column :departments, :slug, :string, after: :position
  end
end
