class ReorderPeopleSlug < ActiveRecord::Migration[5.2]
  def up
    change_column :people, :slug, :string, after: :viewable
  end
end
