class AddDefaultValueToBooleans < ActiveRecord::Migration[5.2]
  def up
    change_column_default :people, :viewable, true
    change_column_default :people, :is_eg_member, false
    change_column_default :people, :email_privacy, false
  end
end
