class AddAsstHeadPersonIdToDepartment < ActiveRecord::Migration[5.2]
  def change
    add_column :departments, :asst_head_person_id, :integer
    change_column :departments, :asst_head_person_id, :integer, after: :head_person_id
  end
end
