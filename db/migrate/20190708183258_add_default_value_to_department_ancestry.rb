class AddDefaultValueToDepartmentAncestry < ActiveRecord::Migration[5.2]
  def up
    change_column_default :departments, :ancestry, '0'
  end
end
