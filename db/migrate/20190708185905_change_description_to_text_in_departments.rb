class ChangeDescriptionToTextInDepartments < ActiveRecord::Migration[5.2]
  def up
    change_column :departments, :description, :text
  end

  def down
    change_column :departments, :description, :string
  end
end
