class AddLibguideIdToPeople < ActiveRecord::Migration[5.2]
  def change
    add_column :people, :libguide_id, :string
  end
end
