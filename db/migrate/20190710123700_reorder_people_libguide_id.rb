class ReorderPeopleLibguideId < ActiveRecord::Migration[5.2]
  def up
    change_column :people, :libguide_id, :string, after: :viewable
  end
end
