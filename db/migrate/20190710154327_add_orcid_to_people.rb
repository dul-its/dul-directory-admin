class AddOrcidToPeople < ActiveRecord::Migration[5.2]
  def change
    add_column :people, :orcid, :string
  end
end
