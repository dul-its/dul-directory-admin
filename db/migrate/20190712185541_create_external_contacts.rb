class CreateExternalContacts < ActiveRecord::Migration[5.2]
  def change
    create_table :external_contacts do |t|
      t.string :name
      t.text :description
      t.string :url
      t.timestamps
    end
  end
end
