class CreateExternalExpertise < ActiveRecord::Migration[5.2]
  def change
    create_table :external_expertise do |t|
      t.integer :external_contact_id
      t.integer :subject_area_id

      t.timestamps
    end
    add_index :external_expertise, [:external_contact_id, :subject_area_id], name: :index_expertise_on_external_contact_and_subject_area
  end
end
