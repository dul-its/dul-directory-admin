class AddLibcalIdToPeople < ActiveRecord::Migration[5.2]
  def change
    add_column :people, :libcal_id, :string
  end
end
