class ReorderPeopleLibcalId < ActiveRecord::Migration[5.2]
  def up
    change_column :people, :libcal_id, :string, after: :libguide_id
  end
end
