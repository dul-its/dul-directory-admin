class AddUrlToTrainings < ActiveRecord::Migration[5.2]
  def up
    add_column :trainings, :url, :string
  end
end
