class AddIconUrlToTrainings < ActiveRecord::Migration[5.2]
  def up
    add_column :trainings, :icon_url, :string
  end
end
