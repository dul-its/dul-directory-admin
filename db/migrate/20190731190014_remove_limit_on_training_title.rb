class RemoveLimitOnTrainingTitle < ActiveRecord::Migration[5.2]
  def up
    change_column :trainings, :title, :string, limit: nil
  end

  def down
    change_column :trainings, :title, :string, limit: 60
  end
end

