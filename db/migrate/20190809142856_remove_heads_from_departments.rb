class RemoveHeadsFromDepartments < ActiveRecord::Migration[5.2]
  def change
    remove_column :departments, :head_person_id, :integer
    remove_column :departments, :asst_head_person_id, :integer
  end
end
