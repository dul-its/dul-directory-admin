class AddHeadReferencesToDepartments < ActiveRecord::Migration[5.2]
  def change
    add_reference :departments, :head_person, foreign_key: {to_table: :people, on_delete: :nullify}
    add_reference :departments, :asst_head_person, foreign_key: {to_table: :people, on_delete: :nullify}
  end
end
