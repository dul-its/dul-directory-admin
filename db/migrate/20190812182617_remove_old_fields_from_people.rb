class RemoveOldFieldsFromPeople < ActiveRecord::Migration[5.2]
  def change
    remove_column :people, :url, :string
    remove_column :people, :phone2, :string
    remove_column :people, :fax, :string
  end
end
