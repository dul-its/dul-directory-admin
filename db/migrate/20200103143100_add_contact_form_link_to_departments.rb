class AddContactFormLinkToDepartments < ActiveRecord::Migration[5.2]
  def change
    add_column :departments, :contact_form_url, :string
    add_column :departments, :contact_form_url_label_text, :string
  end
end
