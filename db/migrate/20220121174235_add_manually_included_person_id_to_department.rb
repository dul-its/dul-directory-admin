class AddManuallyIncludedPersonIdToDepartment < ActiveRecord::Migration[5.2]
  def change
    add_column :departments, :manually_included_person_id, :integer
  end
end
