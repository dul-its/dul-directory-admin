class AddFkReferenceForManuallyIncludedPerson < ActiveRecord::Migration[5.2]
  def change
    remove_column :departments, :manually_included_person_id, :integer
    add_reference :departments, :manually_included_person, foreign_key: {to_table: :people, on_delete: :nullify}
  end
end
