class RemovePhotoUrlFromPeople < ActiveRecord::Migration[6.1]
  def change
    remove_column :people, :photo_url, :string
  end
end
