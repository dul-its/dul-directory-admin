class RemovePhotoIdFromPeople < ActiveRecord::Migration[6.1]
  def change
    remove_column :people, :photo_id, :integer
  end
end
