class AddHidePhoneNumberToPeople < ActiveRecord::Migration[6.1]
  def change
    add_column :people, :hide_phone_number, :boolean
  end
end