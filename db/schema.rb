# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `bin/rails
# db:schema:load`. When creating a new database, `bin/rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2022_09_02_182704) do

  create_table "active_storage_attachments", charset: "utf8mb4", force: :cascade do |t|
    t.string "name", null: false
    t.string "record_type", null: false
    t.bigint "record_id", null: false
    t.bigint "blob_id", null: false
    t.datetime "created_at", null: false
    t.index ["blob_id"], name: "index_active_storage_attachments_on_blob_id"
    t.index ["record_type", "record_id", "name", "blob_id"], name: "index_active_storage_attachments_uniqueness", unique: true
  end

  create_table "active_storage_blobs", charset: "utf8mb4", force: :cascade do |t|
    t.string "key", null: false
    t.string "filename", null: false
    t.string "content_type"
    t.text "metadata"
    t.bigint "byte_size", null: false
    t.string "checksum", null: false
    t.datetime "created_at", null: false
    t.string "service_name", null: false
    t.index ["key"], name: "index_active_storage_blobs_on_key", unique: true
  end

  create_table "active_storage_variant_records", charset: "utf8mb4", force: :cascade do |t|
    t.bigint "blob_id", null: false
    t.string "variation_digest", null: false
    t.index ["blob_id", "variation_digest"], name: "index_active_storage_variant_records_uniqueness", unique: true
  end

  create_table "certifications", charset: "utf8mb4", force: :cascade do |t|
    t.integer "person_id"
    t.integer "training_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["person_id", "training_id"], name: "index_certifications_on_person_id_and_training_id"
  end

  create_table "departments", charset: "utf8mb4", force: :cascade do |t|
    t.string "name"
    t.text "description"
    t.string "sap_org_unit", limit: 20
    t.string "head_title"
    t.string "campus_box"
    t.string "physical_address"
    t.string "email"
    t.string "fax", limit: 40
    t.string "phone", limit: 40
    t.string "phone2", limit: 40
    t.string "url"
    t.string "ancestry", default: "0"
    t.integer "position"
    t.string "slug"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "head_person_id"
    t.bigint "asst_head_person_id"
    t.string "contact_form_url"
    t.string "contact_form_url_label_text"
    t.bigint "manually_included_person_id"
    t.index ["ancestry"], name: "index_departments_on_ancestry"
    t.index ["asst_head_person_id"], name: "index_departments_on_asst_head_person_id"
    t.index ["head_person_id"], name: "index_departments_on_head_person_id"
    t.index ["manually_included_person_id"], name: "index_departments_on_manually_included_person_id"
  end

  create_table "expertise", charset: "utf8mb4", force: :cascade do |t|
    t.integer "person_id"
    t.integer "subject_area_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["person_id", "subject_area_id"], name: "index_expertise_on_person_id_and_subject_area_id"
  end

  create_table "external_contacts", charset: "utf8mb4", force: :cascade do |t|
    t.string "name"
    t.text "description"
    t.string "url"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "external_expertise", charset: "utf8mb4", force: :cascade do |t|
    t.integer "external_contact_id"
    t.integer "subject_area_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["external_contact_id", "subject_area_id"], name: "index_expertise_on_external_contact_and_subject_area"
  end

  create_table "fluencies", charset: "utf8mb4", force: :cascade do |t|
    t.integer "person_id"
    t.integer "language_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["person_id", "language_id"], name: "index_fluencies_on_person_id_and_language_id"
  end

  create_table "languages", charset: "utf8mb4", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "people", charset: "utf8mb4", force: :cascade do |t|
    t.string "first_name", limit: 40
    t.string "middle_name", limit: 40
    t.string "last_name", limit: 40
    t.string "nickname", limit: 40
    t.string "display_name", limit: 80
    t.string "pronouns"
    t.string "net_id", limit: 20
    t.string "unique_id", limit: 20
    t.string "title"
    t.string "preferred_title"
    t.boolean "is_eg_member", default: false
    t.string "ldap_dn"
    t.string "sap_org_unit", limit: 20
    t.string "primary_affiliation", limit: 40
    t.string "affiliation", limit: 40
    t.string "campus_box"
    t.string "physical_address"
    t.string "email"
    t.string "phone", limit: 40
    t.boolean "email_privacy", default: false
    t.text "profile"
    t.boolean "viewable", default: true
    t.string "orcid"
    t.string "libguide_id"
    t.string "libcal_id"
    t.string "slug"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "roles", charset: "utf8mb4", force: :cascade do |t|
    t.integer "department_id"
    t.integer "person_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["department_id", "person_id"], name: "index_roles_on_department_id_and_person_id"
  end

  create_table "subject_areas", charset: "utf8mb4", force: :cascade do |t|
    t.string "title"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "trainings", charset: "utf8mb4", force: :cascade do |t|
    t.string "title"
    t.string "description"
    t.boolean "active"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "url"
    t.string "icon_url"
  end

  create_table "users", charset: "utf8mb4", force: :cascade do |t|
    t.string "username", default: "", null: false
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.integer "role", default: 0
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["username"], name: "index_users_on_username", unique: true
  end

  create_table "versions", charset: "utf8mb4", force: :cascade do |t|
    t.string "item_type", limit: 191, null: false
    t.bigint "item_id", null: false
    t.string "event", null: false
    t.string "whodunnit"
    t.text "object", size: :long
    t.text "object_changes", size: :long
    t.datetime "created_at"
    t.index ["item_type", "item_id"], name: "index_versions_on_item_type_and_item_id"
  end

  add_foreign_key "active_storage_attachments", "active_storage_blobs", column: "blob_id"
  add_foreign_key "active_storage_variant_records", "active_storage_blobs", column: "blob_id"
  add_foreign_key "departments", "people", column: "asst_head_person_id", on_delete: :nullify
  add_foreign_key "departments", "people", column: "head_person_id", on_delete: :nullify
  add_foreign_key "departments", "people", column: "manually_included_person_id", on_delete: :nullify
end
