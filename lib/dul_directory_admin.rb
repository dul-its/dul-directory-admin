# frozen_string_literal: true

require 'dul_directory_admin/version'

module DulDirectoryAdmin
  autoload :Configurable, 'dul_directory_admin/configurable'
  include DulDirectoryAdmin::Configurable
end
