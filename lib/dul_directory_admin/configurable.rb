# frozen_string_literal: true

module DulDirectoryAdmin
  module Configurable
    extend ActiveSupport::Concern

    # rubocop:disable Style/FetchEnvVar
    included do
      mattr_accessor :libguide_url do
        ENV['LIBGUIDE_URL'] ||
          'https://guides.library.duke.edu/prf.php?account_id='
      end

      mattr_accessor :missing_photo_url do
        ENV['MISSING_PHOTO_URL'] ||
          'https://library.duke.edu/sites/default/files/dul/' \
          'staff-portraits/devil-missing-photo.jpg'
      end

      mattr_accessor :staff_change_notify_email do
        ENV['STAFF_CHANGE_NOTIFY_EMAIL']
      end

      mattr_accessor :staging_admin_users do
        (ENV['STAGING_ADMIN_USERS'] || 'seanaery@duke.edu, md169@duke.edu, dlc32@duke.edu, ceg16@duke.edu, tcrich@duke.edu').split(', ')
      end

      mattr_accessor :matomo_tracking_id do
        ENV['MATOMO_TRACKING_ID']
      end

      mattr_accessor :department_redirects do
        { 'technical-services' => 'collections-services' }
      end
    end
    # rubocop:enable Style/FetchEnvVar

    module ClassMethods
      def configure
        yield self
      end
    end
  end
end
