# frozen_string_literal: true

# lib/dul_ldap.rb

class DulLdap
  def self.config
    @@config ||= {}
  end

  def self.config=(hash)
    @@config = hash
  end

  def initialize
    @binddn = Rails.configuration.x.dul_ldap.binddn
    @bindpw = Rails.configuration.x.dul_ldap.bindpw
  end

  # bind to a connection
  def bind
    connection.bind
  end

  def connection
    @connection ||= create_connection
  end

  def membership_info(netid)
    # We'll need to change the filter, such that
    # we're only searching on the person's NetID
    filter = Net::LDAP::Filter.eq('uid', netid)

    # Before calling connection.search, we could simply
    # supply a smaller 'attributes' list, instead of
    # using the list provided in config/dul_ldap.rb
    attributes = %w[uid displayName cn isMemberOf]

    connection.search(
      base: @@config[:base],
      attributes: attributes,
      return_result: true,
      filter: filter
    )
  end

  def staff_entries(hash = {})
    filter = Net::LDAP::Filter.construct @@config[:staff_filter]
    # filter = hash[:filter] if hash[:filter]

    attributes = @@config[:attributes]
    # have LDAP return "isMemberOf" if explicitly requested by
    # calling function
    attributes.push('isMemberOf') if hash[:membership]

    entries = connection.search(
      base: @@config[:base],
      attributes: @@config[:attributes],
      return_result: true,
      filter: filter
    )
    entries if entries.length.positive?
  end

  private

  def create_connection
    Net::LDAP.new(
      host: 'ldap.duke.edu',
      port: 636,
      auth: {
        method: :simple,
        username: @binddn,
        password: @bindpw
      },
      base: 'ou=people,dc=duke,dc=edu',
      encryption: {
        method: :simple_tls,
        tls_options: {
          min_version: OpenSSL::SSL::TLS1_3_VERSION,
          max_version: OpenSSL::SSL::TLS1_3_VERSION
        }
      }
    )
  end
end
