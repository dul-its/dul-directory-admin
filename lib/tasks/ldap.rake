# frozen_string_literal: true

namespace :ldap do
  desc 'Check if a given netID is in the Perkins/staff group'
  task :check_membership, [:nid] => :environment do |_t, args|
    include PeopleHelper

    unless args[:nid]
      puts 'You must provide a netID. Usage: rake ldap:check_membership[nid]'
      next
    end

    if PeopleHelper.in_ldap?(args[:nid])
      puts "The user with netID: #{args[:nid]} is in the Perkins/staff group."
    else
      puts "The user with netID: #{args[:nid]} is NOT in the Perkins/staff group."
    end
  end
end
