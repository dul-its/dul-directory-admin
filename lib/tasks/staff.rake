# frozen_string_literal: true

namespace :staff do
  desc 'Update DUL (Staff) Directory'
  task refresh_from_ldap: :environment do
    app = ActionDispatch::Integration::Session.new(Rails.application)
    app.get '/api/refresh_from_ldap'
  end

  desc 'Run Weekly Staff Report'
  task do_weekly_report: :environment do
    app = ActionDispatch::Integration::Session.new(Rails.application)
    app.get '/api/do_weekly_report'
  end

  desc 'Check membership'
  task check_membership: :environment do
    app = ActionDispatch::Integration::Session.new(Rails.application)
    app.get '/api/check_membership'
  end
end
