# frozen_string_literal: true

PHOTO_PATH = './lib/assets/images/staff_photos'

namespace :staff_photo do
  desc 'Export staff photos from ActiveStorage.'
  task export: :environment do
    Person.find_each do |p|
      next unless p.staff_photo.attached?

      file_ext = p.staff_photo.filename.to_s.split('.').last
      file_ext = 'jpg' if file_ext == 'jpeg'
      image = p.staff_photo.download
      File.open("#{PHOTO_PATH}/#{p.id}.#{file_ext}", 'w+b') do |f|
        f.write(image)
        puts "Saved: #{f.to_path}"
      end
    end
  end

  desc 'Attach staff_photos to Persons.'
  task attach: :environment do
    Dir.foreach(PHOTO_PATH) do |filename|
      next unless filename.match?(/^.*\.(jpg|png)$/)

      person_id = filename.split('.').first
      file_ext = filename.split('.').last
      person = Person.find(person_id)

      next unless person

      dest_filename = "#{person.first_name}_#{person.last_name}" \
                      "_staff_photo.#{file_ext}".downcase.gsub(/[^a-z_.]/, '')
      person.staff_photo.attach(io: File.open("#{PHOTO_PATH}/#{filename}"), filename: dest_filename)
      puts "Attached #{dest_filename} to #{person.display_name}"
    end
  end
end
