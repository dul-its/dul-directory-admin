# frozen_string_literal: true

# rubocop:disable Metrics/BlockLength
namespace :user do
  desc 'Remover User accounts that no longer have a Person account'
  task cleanup: :environment do
    User.find_each do |u|
      unless Person.find_by(net_id: u.net_id)
        puts "Deleting User: #{u.net_id}"
        u.delete
      end
    end
  end

  namespace :create do
    desc 'Create users for development'
    task development_users: :environment do
      Rake::Task['user:create:dev_admin_user'].invoke
      Rake::Task['user:create:dev_hr_admin_user'].invoke
      Rake::Task['user:create:dev_staff_user'].invoke
      Rake::Task['user:create:dev_subject_manager_user'].invoke
    end

    desc 'Create an admin user for development'
    task dev_admin_user: :environment do
      create_user_account('admin@duke.edu', 'admin@duke.edu', 'password', 2)
    end

    desc 'Create an hr_admin user for development'
    task dev_hr_admin_user: :environment do
      create_user_account('hr_admin@duke.edu', 'hr_admin@duke.edu', 'password', 1)
    end

    desc 'Create an subject_manager user for development'
    task dev_subject_manager_user: :environment do
      create_user_account('subject_manager@duke.edu', 'subject_manager@duke.edu', 'password', 3)
    end

    desc 'Create an staff user for development'
    task dev_staff_user: :environment do
      create_user_account('cl334@duke.edu', 'cl334@duke.edu', 'password')
    end

    desc 'Create admin users for staging'
    task staging_admin_users: :environment do
      DulDirectoryAdmin.staging_admin_users.each do |u|
        if User.find_by(username: u)
          admin_user = User.find_by(username: u)
          admin_user.role = 2
          admin_user.save
        else
          create_user_account(u, u, nil, 2)
        end
      end
    end

    def create_user_account(username, email, password = nil, role = 0)
      u = User.new(
        username: username,
        email: email,
        password: password,
        role: role
      )
      u.save
    end
  end
end
# rubocop:enable Metrics/BlockLength
