# frozen_string_literal: true

require 'rails_helper'

RSpec.describe DepartmentsController do
  before do
    FactoryBot.rewind_sequences
  end

  describe 'GET index' do
    let(:dept) { FactoryBot.create(:department) }

    it 'renders the index template' do
      get :index
      expect(response).to render_template('index')
    end
  end
end
