# frozen_string_literal: true

require 'rails_helper'

RSpec.describe ErrorsController do
  describe 'GET 404' do
    it 'renders the page not found template' do
      get :not_found
      expect(response).to render_template('not_found')
    end
  end
end
