# frozen_string_literal: true

require 'rails_helper'

RSpec.describe ExecutiveGroupController do
  before do
    FactoryBot.rewind_sequences
  end

  describe 'GET index' do
    let(:person) do
      FactoryBot.create(:person,
                        :with_administration_department,
                        :executive_group)
    end

    it 'renders the index template' do
      get :index
      expect(response).to render_template('index')
    end
  end
end
