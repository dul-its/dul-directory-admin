# frozen_string_literal: true

require 'rails_helper'

RSpec.describe HealthController, type: :request do
  describe 'GET health check page' do
    it 'renders a 200 response' do
      get '/health-check'
      expect(response).to have_http_status(200)
    end
  end
end
