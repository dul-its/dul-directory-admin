# frozen_string_literal: true

require 'rails_helper'

RSpec.describe SearchesController do
  describe 'GET search' do
    it 'renders the search template' do
      get :search
      expect(response).to render_template('search')
    end
  end
end
