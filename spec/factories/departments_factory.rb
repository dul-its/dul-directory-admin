# frozen_string_literal: true

FactoryBot.define do
  factory :department do
    sequence(:id) { |n| "1000000#{n}" }
    sequence(:name) { |n| "department-#{n}" }
    sequence(:slug) { |n| "department-slug-#{n}" }
  end

  trait :administration do
    name { 'Administration' }
  end

  trait :description do
    description { "This is the department's description" }
  end

  trait :long_description do
    description do
      "<p>This is the department's description. It has a lot of text in multiple formatted " \
        'paragraphs.</p> <p>The description will get stripped and/or truncated in different ' \
        'contexts throughout the directory. <strong>Caution:</strong> use tags sparingly if ' \
        "you can. <a href='https://library.duke.edu'>Here's a link</a> to the library website. " \
        'This will not always render as a link.</p>'
    end
  end

  trait :head_person do
    after(:create) do |department|
      department.head_person = create(:person)
      department.save
    end
  end

  trait :asst_head_person do
    after(:create) do |department|
      department.asst_head_person = create(:person)
      department.save
    end
  end

  trait :manually_included_person do
    after(:create) do |department|
      department.manually_included_person = create(:person)
      department.save
    end
  end

  trait :people do
    people { build_list :person, 2 }
  end

  trait :ancestry_level_one do
    ancestry { '1' }
  end

  trait :ancestry_level_two do
    ancestry { '1/101' }
  end

  trait :with_ancestry_level_two do
    departments { build_list :department, 3, :ancestry_level_two }
  end
end
