# frozen_string_literal: true

FactoryBot.define do
  factory :expertise do
    person
    subject_area
  end
end