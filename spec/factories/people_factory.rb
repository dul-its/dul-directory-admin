# frozen_string_literal: true

FactoryBot.define do
  factory :person do
    sequence(:slug) { |n| "firstname.lastname.#{n}" }
    sequence(:net_id) { |n| "user#{n}" }

    trait :with_departments do
      departments { build_list :department, 2 }
    end

    trait :with_administration_department do
      departments { build_list :department, 1, :administration }
    end

    trait :photo do
      staff_photo_url { '/path/to/a/staff/photo.jpg' }
    end

    trait :activestorage_photo do
      staff_photo { Rack::Test::UploadedFile.new('spec/support/assets/test.jpg', 'image/jpeg') }
    end

    trait :executive_group do
      is_eg_member { true }
    end

    trait :with_subject_areas do
      after(:create) do |person|
        3.times do
          create(:expertise, person: person, subject_area: create(:subject_area))
        end
      end
    end

    trait :with_philosophy_subject_area do
      after(:create) do |person|
        create(:expertise, person: person, subject_area: create(:subject_area, :philosophy))
      end
    end

    trait :with_trainings do
      trainings { build_list :training, 1 }
    end

    trait :with_pride_training do
      trainings { build_list :training, 1, :pride }
    end

    trait :display_name do
      display_name { 'Nickname Lastname' }
    end

    trait :first_name do
      first_name { 'Firstname' }
    end

    trait :last_name do
      last_name { 'Lastname' }
    end

    trait :title do
      title { 'Official Title' }
    end

    trait :preferred_title do
      preferred_title { 'Preferred Title' }
    end

    trait :with_net_id1 do
      net_id { 'net_id_1' }
    end

    trait :is_not_active do
      active { false }
    end

    trait :invalid_slug do
      slug { '.' }
    end

    trait :long_profile do
      profile do
        '<p>Hello, I have a lot to say about myself and I want to use <strong>tags</strong> ' \
          'to make my page look really good and <a href=\'https://library.duke.edu\'>link to</a> ' \
          'lots of resources online.</p> <p>And I have multiple paragraphs of things to say. I ' \
          'know that my full profile will get truncated in some places where it is rendered.</p>'
      end
    end
  end
end
