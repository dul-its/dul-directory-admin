# frozen_string_literal: true

FactoryBot.define do
  factory :role do
    department
    person
  end
end