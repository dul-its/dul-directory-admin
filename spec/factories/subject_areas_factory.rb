# frozen_string_literal: true

FactoryBot.define do
  factory :subject_area do
    sequence(:id) { |n| "100000#{n}" }
    sequence(:title) { |n| "subject-area-#{n}" }
  end

  trait :philosophy do
    title { 'Philosophy' }
  end
end
