# frozen_string_literal: true

FactoryBot.define do
  factory :training do
    sequence(:title) { |n| "training-#{n}" }
    sequence(:url) { |n| "training-url-#{n}" }
  end

  trait :pride do
    title { 'Duke P.R.I.D.E.' }
  end
end
