# frozen_string_literal: true

FactoryBot.define do
  factory :user do
    sequence(:username) { |n| "user#{n}@duke.edu" }
    sequence(:email) { |n| "user#{n}@duke.edu" }
    sequence(:password) { |n| "password#{n}" }

    trait :subject_manager do
      role { 3 }
    end

    trait :admin do
      role { 2 }
    end

    trait :hr_admin do
      role { 1 }
    end

    trait :staff do
      role { 0 }
    end

    trait :with_net_id1 do
      username { 'net_id_1@duke.edu' }
    end
  end
end
