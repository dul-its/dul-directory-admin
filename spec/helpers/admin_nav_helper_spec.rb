# frozen_string_literal: true

describe AdminNavHelper do
  describe '#current_user_greeting' do
    let(:person) { FactoryBot.create(:person, :with_net_id1) }
    let(:user) { FactoryBot.create(:user, :with_net_id1) }
    let(:admin_user) { FactoryBot.create(:user, :admin) }
    let(:hr_admin_user) { FactoryBot.create(:user, :hr_admin) }
    let(:subject_manager_user) { FactoryBot.create(:user, :subject_manager) }

    it 'returns a display name for a staff user' do
      expect(helper.current_user_greeting(user)).to(
        match(/Hello user\d+/)
      )
    end

    it 'returns a display name for an admin user' do
      expect(helper.current_user_greeting(admin_user)).to(
        match(/Hello user\d+ \(admin\)/)
      )
    end

    it 'returns a display name for an hr_admin user' do
      expect(helper.current_user_greeting(hr_admin_user)).to(
        match(/Hello user\d+ \(hr_admin\)/)
      )
    end

    it 'returns a display name for an subject_manager user' do
      expect(helper.current_user_greeting(subject_manager_user)).to(
        match(/Hello user\d+ \(subject_manager\)/)
      )
    end
  end

  describe '#current_user_link' do
    let(:person) { FactoryBot.create(:person, :with_net_id1) }
    let(:user) { FactoryBot.create(:user, :with_net_id1) }
    let(:admin_user) { FactoryBot.create(:user, :admin) }

    it 'returns a link to the current person record' do
      expect(helper.current_user_link(user, person.id)).to(
        match(%r{/admin/person/\d+})
      )
    end

    it 'absent a person, returns a link to the current user record' do
      expect(helper.current_user_link(admin_user)).to(
        match(%r{/admin/user/\d+})
      )
    end
  end

  describe '#edit_your_profile_link' do
    let(:person) { FactoryBot.create(:person, :with_net_id1) }
    let(:user) { FactoryBot.create(:user, :with_net_id1) }
    let(:admin_user) { FactoryBot.create(:user, :admin) }

    it 'returns a link to edit the current person record' do
      expect(helper.edit_your_profile_link(user, person.id)).to(
        match(%r{/admin/person/\d+/edit})
      )
    end

    it 'absent a person, returns a link to edit the current user record' do
      expect(helper.edit_your_profile_link(admin_user)).to(
        match(%r{/admin/user/\d+/edit})
      )
    end
  end
end
