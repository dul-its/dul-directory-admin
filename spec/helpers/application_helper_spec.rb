# frozen_string_literal: true

describe ApplicationHelper do
  describe '#staff_photo missing' do
    let(:person) { FactoryBot.create(:person, :with_departments) }

    it 'returns a missing photo' do
      expect(helper.staff_photo(person: person)).to(
        eq('<div class="photo"><img loading="lazy" style="" class="media-objec'\
           't" alt="" src="https://library.duke.edu/sites/default/files/dul/st'\
           'aff-portraits/devil-missing-photo.jpg" /></div>')
      )
    end
  end

  describe '#staff_photo with url' do
    let(:person) { FactoryBot.create(:person, :with_departments, :activestorage_photo, :executive_group) }

    it 'returns a staff photo' do
      expect(helper.staff_photo(person: person)).to(
        include('staff_photo.jpg')
      )
    end

    it 'does not return a missing staff photo' do
      expect(helper.staff_photo(person: person)).not_to(
        include('devil-missing-photo.jpg')
      )
    end
  end

  describe '#org_chart_link' do
    it 'returns a link to the DUL org chart' do
      expect(helper.org_chart_link).to(
        eq('<a href="https://library.duke.edu/about/orgchart">' \
           '<i aria-hidden="true" class="far fa-file-pdf"></i>' \
           ' Organizational Chart <span class="sr-only">(PDF)</span></a>')
      )
    end
  end
end
