# frozen_string_literal: true

describe DepartmentHelper do
  before do
    FactoryBot.rewind_sequences
  end

  describe '#display_department_head' do
    let(:person) { FactoryBot.create(:person, :display_name, :title) }

    it 'returns the appropriate markup' do
      expect(helper.display_department_head(person)).to(
        eq('<div class="dept-head"><span class="dept-head-name">Nickname '\
           'Lastname</span><span class="dept-head-title">Official Title</span>'\
           '</div>')
      )
    end
  end

  describe '#department_ancestry_class' do
    let(:dept) { FactoryBot.create(:department, :ancestry_level_two) }

    it 'returns the appropriate classes' do
      expect(helper.department_ancestry_class(dept.ancestry)).to(
        eq('d-flex flex-column department dept-2')
      )
    end
  end

  describe '#department_info_present' do
    let(:dept) { FactoryBot.create(:department, :description) }

    it 'returns true if it has any info' do
      expect(helper.department_info_present(dept)).to(
        eq(true)
      )
    end
  end

  describe '#dept_og_description' do
    context 'with no description' do
      let(:dept) { FactoryBot.create(:department) }

      it 'returns an empty string' do
        expect(helper.dept_og_description(dept)).to eq('')
      end
    end

    context 'with a long description with HTML tags' do
      let(:dept) { FactoryBot.create(:department, :long_description) }

      # rubocop:disable RSpec/ExampleLength
      it 'strips the tags and truncates the profile text' do
        expect(helper.dept_og_description(dept)).to eq(
          'This is the department\'s description. It has a lot of text in multiple formatted ' \
          'paragraphs. The description will get stripped and/or truncated in different ' \
          'contexts throughout the directory. Caution: use tags sparingly if you can. Here\'s ' \
          'a link to the library website. This will not always render as a...'
        )
      end
      # rubocop:enable RSpec/ExampleLength
    end
  end
end
