# frozen_string_literal: true

describe PeopleHelper do
  before do
    FactoryBot.rewind_sequences
  end

  describe '#edit_person_button' do
    let(:person) { FactoryBot.create(:person, :with_net_id1) }
    let(:other_person) { FactoryBot.create(:person) }
    let(:user) { FactoryBot.create(:user, :with_net_id1) }

    it 'returns a button link for the current user to edit their profile' do
      expect(helper.edit_person_button(person, user)).to(
        match(%r{<a class="btn btn-primary" href="/admin/person/\d+/edit">Edit Profile</a>})
      )
    end

    it 'returns nil if the person and user are not the same' do
      expect(helper.edit_person_button(other_person, user)).to be_nil
    end
  end

  describe '#person_subject_areas' do
    let(:person) { FactoryBot.create(:person, :with_subject_areas) }

    it 'returns an HTML formatted list of subject areas' do
      expect(helper.person_attribute_list(person: person, attribute: 'subject-areas')).to(
        eq('<li><a href="/subject-specialists#1000001">subject-area-1</a></li>' \
           '<li><a href="/subject-specialists#1000002">subject-area-2</a></li>' \
           '<li><a href="/subject-specialists#1000003">subject-area-3</a></li>')
      )
    end
  end

  describe '#person_og_title' do
    context 'with name & preferred title' do
      let(:person) { FactoryBot.create(:person, :display_name, :preferred_title) }

      it 'returns the display name & preferred title separated by pipe' do
        expect(helper.person_og_title(person)).to eq('Nickname Lastname | Preferred Title')
      end
    end

    context 'with name but no job title' do
      let(:person) { FactoryBot.create(:person, :display_name) }

      it 'returns the display name only' do
        expect(helper.person_og_title(person)).to eq('Nickname Lastname')
      end
    end
  end

  describe '#person_og_description' do
    context 'with no profile' do
      let(:person) { FactoryBot.create(:person) }

      it 'returns an empty string' do
        expect(helper.person_og_description(person)).to eq('')
      end
    end

    context 'with a long profile with HTML tags' do
      let(:person) { FactoryBot.create(:person, :long_profile) }

      it 'strips the tags and truncates the profile text' do
        expect(helper.person_og_description(person)).to eq(
          'Hello, I have a lot to say about myself and I want to use tags to make my page look ' \
          'really good and link to lots of resources online. And I have multiple paragraphs of ' \
          'things to say. I know that my full profile will get truncated in some...'
        )
      end
    end
  end

  describe '#person_profile' do
    context 'with no profile' do
      let(:person) { FactoryBot.create(:person) }

      it 'returns nil' do
        expect(helper.person_profile_display(person)).to eq(nil)
      end
    end

    context 'with a long profile' do
      let(:person) { FactoryBot.create(:person, :long_profile) }

      # rubocop:disable RSpec/ExampleLength
      it 'returns all of the profile content including html tags' do
        expect(helper.person_profile_display(person)).to eq(
          '<p>Hello, I have a lot to say about myself and I want to use <strong>tags</strong> ' \
          'to make my page look really good and <a href=\'https://library.duke.edu\'>link to</a> ' \
          'lots of resources online.</p> <p>And I have multiple paragraphs of things to say. I ' \
          'know that my full profile will get truncated in some places where it is rendered.</p>'
        )
      end
      # rubocop:enable RSpec/ExampleLength
    end

    # TODO: mock up a json response to test scholars@duke profile content
  end
end
