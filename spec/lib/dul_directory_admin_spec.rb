# frozen_string_literal: true

RSpec.describe DulDirectoryAdmin do
  describe 'configuration defaults' do
    subject(:configuration) { described_class }

    it 'has a libguide_url' do
      expect(configuration.libguide_url).to(
        eq('https://guides.library.duke.edu/prf.php?account_id=')
      )
    end

    it 'has a missing_photo_url' do
      expect(configuration.missing_photo_url).to(
        eq('https://library.duke.edu/sites/default/files/dul/' \
           'staff-portraits/devil-missing-photo.jpg')
      )
    end
  end
end
