# frozen_string_literal: true

RSpec.describe NoticeMailer, type: :mailer do
  describe '#email_staff_photo_updated' do
    let(:mail) do
      described_class.with(slug: 'slug',
                           net_id: 'net_id',
                           display_name: 'Display Name')
                     .email_staff_photo_updated
    end

    before do
      DulDirectoryAdmin.staff_change_notify_email = 'test-email@duke.edu'
    end

    it 'has a subject' do
      expect(mail.subject).to(
        eq('Staff Directory photo changed: Display Name (net_id)')
      )
    end

    it 'has a to address' do
      expect(mail.to).to eq(['test-email@duke.edu'])
    end

    it 'has a from address' do
      expect(mail.from).to eq(['library-system-administration@duke.edu'])
    end

    it 'renders the body' do
      expect(mail.body.encoded).to(
        include('The staff photo for Display Name (net_id) has been updated.')
      )
    end
  end

  describe '#do_weekly_report' do
    let(:mail) do
      people_no_title = ["Mock User doesn't have a preferred title"]
      people_no_dept = []
      described_class.with(people_no_title: people_no_title,
                           people_no_dept: people_no_dept)
                     .weekly_report_email
    end

    before do
      DulDirectoryAdmin.staff_change_notify_email = 'test-email@duke.edu'
    end

    it 'has a subject' do
      expect(mail.subject).to(
        eq('Weekly Report')
      )
    end

    it 'renders the body' do
      expect(mail.body.encoded).to(
        include("Mock User doesn't have a preferred title")
      )
    end
  end
end
