# frozen_string_literal: true

require 'rails_helper'
require 'cancan/matchers'

RSpec.describe Ability do
  subject(:ability) { described_class.new(user) }

  describe 'admin user abilities' do
    let(:user) { FactoryBot.create(:user, :admin) }

    it { is_expected.to be_able_to(:read, :dashboard) }
    it { is_expected.to be_able_to(:access, :rails_admin) }

    [Department,
     ExternalContact,
     Language,
     SubjectArea,
     Training,
     User].each do |model|
      it { is_expected.to be_able_to(:manage, model) }
    end

    it { is_expected.to be_able_to(:read, Person) }
    it { is_expected.to be_able_to(:update, Person) }
    it { is_expected.not_to be_able_to(:create, Person) }
    it { is_expected.not_to be_able_to(:destroy, Person) }
  end

  describe 'hr_admin user abilities' do
    let(:user) { FactoryBot.create(:user, :hr_admin) }

    it { is_expected.to be_able_to(:read, :dashboard) }
    it { is_expected.to be_able_to(:access, :rails_admin) }

    [Department, Training].each do |model|
      it { is_expected.to be_able_to(:manage, model) }
    end

    it { is_expected.to be_able_to(:read, Person) }
    it { is_expected.to be_able_to(:update, Person) }
    it { is_expected.not_to be_able_to(:create, Person) }
    it { is_expected.not_to be_able_to(:destroy, Person) }

    [ExternalContact, Language, SubjectArea, User].each do |model|
      it { is_expected.to be_able_to(:read, model) }
      it { is_expected.not_to be_able_to(:create, model) }
      it { is_expected.not_to be_able_to(:update, model) }
      it { is_expected.not_to be_able_to(:destroy, model) }
    end
  end

  describe 'subject_manager user abilities' do
    let(:user) { FactoryBot.create(:user, :subject_manager) }

    it { is_expected.to be_able_to(:read, :dashboard) }
    it { is_expected.to be_able_to(:access, :rails_admin) }

    [Expertise, ExternalContact, SubjectArea].each do |model|
      it { is_expected.to be_able_to(:manage, model) }
    end

    it { is_expected.to be_able_to(:read, Person) }
    it { is_expected.to be_able_to(:update, Person) }
    it { is_expected.not_to be_able_to(:create, Person) }
    it { is_expected.not_to be_able_to(:destroy, Person) }
  end

  describe 'staff user abilities' do
    before do
      FactoryBot.rewind_sequences
    end

    let(:user) { FactoryBot.create(:user, :staff, :with_net_id1) }
    let(:person_net_id1_active) { FactoryBot.create(:person, :with_net_id1) }
    let(:person_net_id1_not_active) do
      FactoryBot.create(:person, :with_net_id1, :is_not_active)
    end
    let(:other_person) { FactoryBot.create(:person) }

    it { is_expected.to be_able_to(:read, :dashboard) }
    it { is_expected.to be_able_to(:access, :rails_admin) }

    it { is_expected.not_to be_able_to(:create, Person) }

    # can read and update, but not destroy person entry
    # where the Person.net_id matches the User.username
    it { is_expected.to be_able_to(:read, person_net_id1_active) }
    it { is_expected.to be_able_to(:update, person_net_id1_active) }
    it { is_expected.not_to be_able_to(:destroy, person_net_id1_active) }

    # cannot read, update or destroy any person entry
    # where the Person.active is false even if
    # the Person.net_id matches the User.username
    it { is_expected.not_to be_able_to(:read, person_net_id1_not_active) }
    it { is_expected.not_to be_able_to(:update, person_net_id1_not_active) }
    it { is_expected.not_to be_able_to(:destroy, person_net_id1_not_active) }

    # cannot read, update or destroy any person entry
    # where the Person.net_id does match the User.username
    it { is_expected.not_to be_able_to(:read, other_person) }
    it { is_expected.not_to be_able_to(:update, other_person) }
    it { is_expected.not_to be_able_to(:destroy, other_person) }

    # rubocop:disable RSpec/RepeatedExample
    [Language, SubjectArea, Training].each do |model|
      it { is_expected.to be_able_to(:read, model) }
      it { is_expected.not_to be_able_to(:create, model) }
      it { is_expected.not_to be_able_to(:update, model) }
      it { is_expected.not_to be_able_to(:destroy, model) }
    end

    [Department, ExternalContact, User].each do |model|
      it { is_expected.not_to be_able_to(:read, model) }
      it { is_expected.not_to be_able_to(:create, model) }
      it { is_expected.not_to be_able_to(:update, model) }
      it { is_expected.not_to be_able_to(:destroy, model) }
    end
    # rubocop:enable RSpec/RepeatedExample
  end
end
