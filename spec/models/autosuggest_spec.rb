# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Autosuggest do
  describe '.suggestions' do
    it 'returns a list of person and department names' do
      expect(described_class.suggestions).to be_an_instance_of(Array)
    end
  end
end
