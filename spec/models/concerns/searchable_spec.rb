# frozen_string_literal: true

require 'spec_helper'

class TestClass
  include Searchable

  def self.exact_search(_query); end

  def self.like_search(_query); end
end

describe Searchable do
  let(:test_class) { TestClass }

  describe '.search' do
    it 'implements the search class method' do
      expect(test_class).to respond_to(:search)
    end
  end

  describe 'sanitize_query' do
    it 'implements the sanitize_query class method' do
      expect(test_class).to respond_to(:sanitize_query)
    end

    it 'removes unwanted characters' do
      expect(test_class.sanitize_query("abcABC&*#^@%{}[]123'")).to(
        eq("abcABC&123'")
      )
    end
  end
end
