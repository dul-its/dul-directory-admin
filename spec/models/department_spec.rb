# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Department, type: :model do
  it 'implements the exact_search class method' do
    expect(described_class).to respond_to(:exact_search)
  end

  it 'implements the like_search class method' do
    expect(described_class).to respond_to(:like_search)
  end

  describe '#all_staff_for_display' do
    let(:dept) do
      FactoryBot.create(:department,
                        :head_person,
                        :asst_head_person,
                        :manually_included_person,
                        :people)
    end

    it 'assembles a list of staff for display' do
      expect(dept.all_staff_for_display).to(
        eq([dept.head_person, dept.asst_head_person,
            dept.manually_included_person, dept.people.first,
            dept.people.last])
      )
    end
  end
end
