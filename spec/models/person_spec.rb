# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Person, type: :model do
  it 'implements the exact_search class method' do
    expect(described_class).to respond_to(:exact_search)
  end

  it 'implements the like_search class method' do
    expect(described_class).to respond_to(:like_search)
  end

  describe 'Staff person' do
    let(:person) { FactoryBot.create(:person, :with_departments) }

    it 'is valid with valid attributes' do
      expect(person).to be_valid
    end

    it 'is not an EG member' do
      expect(person.is_eg_member).to eq(false)
    end
  end

  describe 'Executive group person' do
    let(:person) { FactoryBot.create(:person, :executive_group) }

    it 'is member of EG' do
      expect(person.is_eg_member).to eq(true)
    end

    it 'is head librarian' do
      expect(person.head_of_library?).to eq(false)
    end
  end

  describe 'Head of library person' do
    let(:person) do
      FactoryBot.create(:person,
                        :with_administration_department,
                        :executive_group)
    end

    it 'is member of EG' do
      expect(person.is_eg_member).to eq(true)
    end

    it 'is head librarian' do
      expect(person.head_of_library?).to eq(true)
    end
  end

  describe '.is_active' do
    let(:active_person) { FactoryBot.create(:person) }
    let(:not_active_person) { FactoryBot.create(:person, :is_not_active) }

    it 'includes people with active flag' do
      expect(described_class.is_active).to include(active_person)
    end

    it 'does not include people without the active flag' do
      expect(described_class.is_active).not_to include(not_active_person)
    end
  end

  describe '.is_valid' do
    let(:valid_person) { FactoryBot.create(:person, :first_name, :last_name, :display_name) }
    let(:person_no_name) { FactoryBot.create(:person) }
    let(:person_invalid_slug) { FactoryBot.create(:person, :invalid_slug) }

    it 'includes people with valid and required fields' do
      expect(described_class.is_valid).to include(valid_person)
    end

    it 'does not include people without names' do
      expect(described_class.is_valid).not_to include(person_no_name)
    end

    it 'does not include people with invalid slugs' do
      expect(described_class.is_valid).not_to include(person_invalid_slug)
    end
  end

  describe '#display_title' do
    let(:person) { FactoryBot.create(:person, :title) }
    let(:person_pref) { FactoryBot.create(:person, :title, :preferred_title) }

    it 'returns the default staff title' do
      expect(person.display_title).to(
        eq('Official Title')
      )
    end

    it 'returns the preferred staff title' do
      expect(person_pref.display_title).to(
        eq('Preferred Title')
      )
    end
  end

  describe '#dept_head?' do
    let(:dept) { FactoryBot.create(:department, :head_person) }
    let(:head_person) { dept.head_person }
    let(:staff_person) { FactoryBot.create(:person) }

    # before do
    #   head_person.departments << dept
    #   staff_person.departments << dept
    # end

    before do
      FactoryBot.create(:role, department: dept, person: head_person)
      FactoryBot.create(:role, department: dept, person: staff_person)
    end

    it 'returns true if the person is a department head' do
      expect(head_person.dept_head?).to be true
    end

    it 'returns false if the person is not a deptartment head' do
      expect(staff_person.dept_head?).to be false
    end
  end

  describe '#asst_dept_head?' do
    let(:dept) { FactoryBot.create(:department, :asst_head_person) }
    let(:asst_head_person) { dept.asst_head_person }
    let(:staff_person) { FactoryBot.create(:person) }

    # before do
    #   asst_head_person.departments << dept
    #   staff_person.departments << dept
    # end

    before do
      FactoryBot.create(:role, department: dept, person: asst_head_person)
      FactoryBot.create(:role, department: dept, person: staff_person)
    end

    it 'returns true if the person is an asst department head' do
      expect(asst_head_person.asst_dept_head?).to be true
    end

    it 'returns false if the person is not an asst department head' do
      expect(staff_person.dept_head?).to be false
    end
  end
end
