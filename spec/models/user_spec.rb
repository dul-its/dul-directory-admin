# frozen_string_literal: true

require 'rails_helper'

RSpec.describe User, type: :model do
  subject(:user) { FactoryBot.create(:user) }

  it 'has a username' do
    expect(user.username).to be_present
  end

  it 'has an email' do
    expect(user.email).to be_present
  end

  it 'has a password' do
    expect(user.password).to be_present
  end

  it 'has a default role of staff' do
    expect(user.role).to eq('staff')
  end

  describe '#admin?' do
    subject(:admin_user) { FactoryBot.create(:user, :admin) }

    it 'returns true if role is admin' do
      expect(admin_user.admin?).to be true
    end

    it 'returns false if role is not admin' do
      expect(user.admin?).to be false
    end
  end

  describe '#hr_admin?' do
    subject(:hr_admin_user) { FactoryBot.create(:user, :hr_admin) }

    it 'returns true if role is hr_admin' do
      expect(hr_admin_user.hr_admin?).to be true
    end

    it 'returns false if role is not hr_admin' do
      expect(user.hr_admin?).to be false
    end
  end

  describe '#subject_manager?' do
    subject(:subject_manager_user) { FactoryBot.create(:user, :subject_manager) }

    it 'returns true if role is subject_manager' do
      expect(subject_manager_user.subject_manager?).to be true
    end

    it 'returns false if role is not subject_manager' do
      expect(user.subject_manager?).to be false
    end
  end

  describe '#staff?' do
    subject(:admin_user) { FactoryBot.create(:user, :admin) }

    it 'returns true if role is staff' do
      expect(user.staff?).to be true
    end

    it 'returns false if role is not staff' do
      expect(admin_user.staff?).to be false
    end
  end
end
