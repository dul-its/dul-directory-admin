# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'Browse requests', type: :request do
  it 'redirects /browse to /' do
    get '/browse'
    expect(response).to redirect_to('/')
  end

  it 'redirects /browse/all to /' do
    get '/browse/all'
    expect(response).to redirect_to('/')
  end

  it 'redirects /staff to /' do
    get '/staff'
    expect(response).to redirect_to('/')
  end
end
