# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'GET Executive Group index', type: :request do
  before do
    FactoryBot.rewind_sequences
  end

  let(:person) { FactoryBot.create(:person, :with_departments, :with_photo) }

  it 'uses the EG page template' do
    get '/executive-group'
    expect(response).to render_template(:index)
  end

  it 'has the correct H1' do
    get '/executive-group'
    expect(response.body).to include('<h1 class="page-title" id="dept-name" itemprop="name">Library Executive Group</h1>')
  end
end
