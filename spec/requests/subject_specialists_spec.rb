# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'GET Subject Specialists index', type: :request do
  before do
    FactoryBot.rewind_sequences
  end

  let(:person) { FactoryBot.create(:person, :with_subject_areas, :with_photo) }

  # # FAILING TEST:
  # # ActionView::Template::Error:
  # # Unknown primary key for table subject_areas in model SubjectArea.
  # it 'uses the Subject Specialists page template' do
  #   get '/subject-specialists'
  #   expect(response).to render_template(:index)
  # end

  it 'has the correct H1' do
    get '/subject-specialists'
    expect(response.body).to include('<h1 class="page-title" id="dept-name" itemprop="name">Subject Specialists</h1>')
  end
end
