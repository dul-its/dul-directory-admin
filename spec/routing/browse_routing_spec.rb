# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'routes for Browse', type: :routing do
  it 'routes / to browse#index' do
    expect(get('/')).to route_to('browse#index')
  end

  it 'routes /browse/a to browse#show' do
    expect(get('browse/a')).to(
      route_to({ 'controller' => 'browse',
                 'action' => 'show',
                 'id' => 'a' })
    )
  end
end
