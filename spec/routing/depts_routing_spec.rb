# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'routes for Depts', type: :routing do
  it 'routes /dept to dept#index' do
    expect(get('/dept')).to route_to('departments#index')
  end

  it 'routes /dept/a to dept#show' do
    expect(get('dept/a')).to(
      route_to({ 'controller' => 'departments',
                 'action' => 'show',
                 'slug' => 'a' })
    )
  end
end
