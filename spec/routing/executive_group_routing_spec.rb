# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'routes for Executive Group', type: :routing do
  it 'routes /executive-group to the EG controller' do
    expect(get('/executive-group'))
      .to route_to('executive_group#index')
  end
end
