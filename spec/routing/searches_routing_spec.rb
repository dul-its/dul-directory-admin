# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'routes for Searches', type: :routing do
  it 'routes /search to searches#search' do
    expect(get('/search')).to route_to('searches#search')
  end
end
