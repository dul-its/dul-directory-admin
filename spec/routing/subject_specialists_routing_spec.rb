# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'routes for Subject Specialists', type: :routing do
  it 'routes /subject-specialists to the Subject Specialists controller' do
    expect(get('/subject-specialists'))
      .to route_to('subject_specialists#index')
  end
end
